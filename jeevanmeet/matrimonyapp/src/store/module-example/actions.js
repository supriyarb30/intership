// export function someAction (/* context */) {
// }
// export function someAction (/* context */) {
// }
import { api } from 'boot/axios'

export function setLogoutTimer ({commit}, expirationTime) {
    setTimeout(() => {
      commit('clearAuthData')
    }, expirationTime * 1000)
  }

  


export function  login ({commit, dispatch}, authData) {

  const p = new Promise(function (resolve, reject) {
    api.post('token/', {
      username: authData.username,
      password: authData.password,
      returnSecureToken: true
    })
      .then(res => {
        const now = new Date()
        localStorage.setItem('token', res.data.access)
        localStorage.setItem('refresh_token', res.data.refresh)

        commit('authUser', {
          token: res.data.access,
          refresh_token: res.data.refresh
        })

        // set token to headers
        const token = res.data.access
        console.log('Token --'+token)
        api.defaults.headers.common['Authorization'] =
          'Bearer ' + token

       
        resolve()
      })
      .catch(error => { console.log(error);
        reject(error)
        console.log('i am herer *****');
      
      });

    });
    return p
  }


export function  tryAutoLogin ({commit}) {
    const token = localStorage.getItem('token')
    if (!token) {
      return
    }
    const expirationDate = localStorage.getItem('expirationDate')
    const now = new Date()
    if (now >= expirationDate) {
      return
    }
    const userId = localStorage.getItem('userId')
    commit('authUser', {
      token: token,
      userId: userId
    })
  }


export function logout ({commit}) {

    commit('clearAuthData')
    localStorage.removeItem('token')
    localStorage.removeItem('refresh_token')
    localStorage.removeItem('permissions')
    delete api.defaults.headers.common['Authorization']
    //router.replace('/')

  }