// export default function () {
//   return {
//     //
//   }
// }
export default function () {
  return {
    token: localStorage.getItem('token') || '',
    refresh_token: localStorage.getItem('refresh_token') || '',
    permissions:(localStorage.getItem('permissions') || '')
  }
}