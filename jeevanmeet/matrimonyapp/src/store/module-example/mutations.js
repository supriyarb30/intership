// export function someMutation (/* state */) {
// }
export function authUser (state, userData) {
    state.token = userData.token
    state.refresh_token = userData.refresh_token
  }

export function storePermissions(state, permission_data){
    state.permissions = permission_data
}
 
export function storeUser (state, user) {
    state.user = user
  }

export function  clearAuthData (state) {
    state.token = null
    state.refresh_token = null
    state.permissions = null
  }

  