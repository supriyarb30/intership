
const routes = [
//{ path: '/', component: () => import('pages/Login.vue') },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
    {
       path: '/',
      component: () => import('pages/Index.vue'),
      name:'PageIndex' },

    

      
      { path: '/demopage', component: () => import('pages/demo_page.vue') },
      { path: '/demolist', component: () => import('pages/demo_list.vue') },
      
  
    
      { path: '/login', component: () => import('pages/Login.vue') },
      { path: '/myprofile', component: () => import('pages/myprofile.vue') },
      { path: '/register', 
      component: () => import('pages/register.vue') },

      { path:'messenger',
       component: () => import('src/pages/Messenger.vue') },

      { path: 'photo', component: () => import('pages/photo.vue') },


      { path: 'button', component: () => import('pages/button.vue') },
      
      { path: 'vedio', component: () => import('pages/vedio.vue') },
      { path: 'imgcard', component: () => import('pages/imgcard.vue') },
      
      { path: '/drop', component: () => import('pages/drop.vue') },
      { path: '/feedback', component: () => import('pages/Feedback.vue') },
      
    
      
      { path: '/idsearch', component: () => import('pages/Idsearch.vue') },
      { path: '/regularsearch', component: () => import('pages/regularsearch.vue') },
      { path: '/advancesearch', component: () => import('pages/advancesearch.vue') },
      { path: '/interfaithsearch', component: () => import('pages/interfaithsearch.vue') },

      { path: '/franchiesregister', component: () => import('pages/franchise _register.vue') },
      { path: '/franchiessearch', component: () => import('pages/franchise_search.vue') },


      { path: '/bridespage', component: () => import('pages/brides_page.vue') },
      { path: '/viewprofileb/:id/',name:'viewbride', component: () => import('pages/viewprofileb.vue') },
      { path: '/loginviewprofileb', component: () => import('pages/loginviewprofileb.vue') },
      //{ path: '/brideslist', component: () => import('pages/brides_list.vue') },

      { path: '/groomspage', component: () => import('pages/grooms_page.vue') },
      { path: '/viewprofileg/:id/', component: () => import('pages/viewprofileg.vue') },
      { path: '/loginviewprofileg', component: () => import('pages/loginviewprofileg.vue') },
      //{ path: 'groomslist', component: () => import('pages/grooms_list.vue') },
      
      { path: '/membership', component: () => import('pages/membership.vue') },
      { path: '/upgradationdetails/:id/', component: () => import('pages/upgradationdetails.vue') },
      { path: '/upgradauser/:id/', component: () => import('pages/upgradauser.vue') },
                                                              
      { path: '/tellstory', component: () => import('pages/tellstory.vue') },
      { path: 'termsconditions', component: () => import('pages/termsconditions.vue') },
      { path: 'policy', component: () => import('pages/policy.vue') },
      { path: 'faq', component: () => import('pages/faq.vue') },

      { path: '/contact', 
      component: () => import('pages/ContactUs.vue'),
      name:'ConatctUs' },
      { path: 'blog', component: () => import('pages/Blog.vue') },
      
      
      
    ]
  },
 

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes


