from django.forms import ModelForm, Textarea, TextInput
from django import forms
from .models import *


class FranchiseRegisterSearchForm(forms.Form):
	email=forms.EmailField(required=False)
	password=forms.CharField(required=False)
	repassword=forms.CharField(required=False)
	owner_name=forms.CharField(required=False)
	mobile_number=forms.CharField(required=False)
	mobile_number2=forms.CharField(required=False)
	country=forms.CharField(required=False)
	state=forms.CharField(required=False)
	district=forms.CharField(required=False)
	city=forms.CharField(required=False)
	current_address=forms.CharField(required=False)
	images=forms.ImageField(required=False)