from django.shortcuts import render
from .models import *
from django.views.generic import TemplateView, DetailView,CreateView,UpdateView,DetailView,ListView
from .franchise_search import *
from django.urls import reverse
from .forms import *

from django.urls import reverse_lazy
from django.http import HttpResponseRedirect

class FranchiseRegisterList(ListView):
    model = FranchiseRegister
    paginate_by = 10
    template_name = 'franchise/franchies_list.html'
    form_class = FranchiseRegisterSearchForm

    def get_context_data(self, **kwargs):
        context = super(FranchiseRegisterList, self).get_context_data(**kwargs)
        if self.request.GET:
            context['form'] = FranchiseRegisterSearchForm(self.request.GET)
        else:
            context['form'] = FranchiseRegisterSearchForm()
        return context

    def get_queryset(self):
        args = {}
        email = self.request.GET.get('email',False)
        password = self.request.GET.get('password',False)
        repassword = self.request.GET.get('repassword', False)
        franchise_name = self.request.GET.get('franchise_name', False)
        owner_name = self.request.GET.get('owner_name', False)
        mobile_number = self.request.GET.get('mobile_number', False)
        mobile_number2 = self.request.GET.get('mobile_number2', False)
        country = self.request.GET.get('country', False)
        state = self.request.GET.get('state', False)
        district = self.request.GET.get('district', False)
        city = self.request.GET.get('city', False)
        current_address = self.request.GET.get('current_address', False)
        images = self.request.GET.get('images', False)


        if email:
            args['email__icontains'] = email

        if password:
            args['password__icontains'] = password

        if repassword:
            args['repassword__icontains'] = repassword

        if franchise_name:
            args['franchise_name'] = franchise_name
    
        if owner_name:
            args['owner_name'] = owner_name

        if mobile_number:
            args['mobile_number'] = mobile_number

        if country:
            args['country'] = country

        if state:
            args['state'] = state

        if district:
            args['district'] = district

        if city:
            args['city'] = city

        if current_address:
            args['current_address'] = current_address

        if images:
            args['images'] = images


        return FranchiseRegister.objects.filter(**args).order_by('-email')

class FranchiseRegisterCreate(CreateView):
    model = FranchiseRegister
    form_class = FranchiseRegisterForm
    template_name = "franchise/franchise_form.html"
    success_url = reverse_lazy('franchiseregister')
    

    def get_context_data(self, **kwargs):
        context = super(FranchiseRegisterCreate, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        
        if (form.is_valid()):
           
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self,form):
        return self.render_to_response(self.get_context_data(form=form)) 


class FranchiseRegisterUpdate(UpdateView):
    model = FranchiseRegister
    form_class = FranchiseRegisterForm
    template_name="franchise/franchise_form.html"


    def get_success_url(self):
        print("VALID")
        return reverse('franchise')

    def form_valid(self, form):
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        print("INVALID")
        print(form)
        return self.render_to_response(
            self.get_context_data(form=form,
                                  ))

class FranchiseRegisterDetail(DetailView):
    model = FranchiseRegister
    template_name = "franchise/franchise_detail.html"
