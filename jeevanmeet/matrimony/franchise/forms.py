from django.forms import ModelForm
from django import forms
from .models import *
from .forms import *


class FranchiseRegisterForm(ModelForm):
    class Meta:
        model = FranchiseRegister
        fields = ('email','password','repassword','franchise_name','franchise_name','mobile_number','mobile_number2','country','state','district','city','current_address','images')
        # exclude = ('person')