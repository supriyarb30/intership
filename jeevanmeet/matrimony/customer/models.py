from django.db import models

from core.models import *
from django.utils import timezone

# Create your models here.
class PersonalDetails(models.Model):
	user_id = models.AutoField(primary_key=True)
	#users = models.OneToOneField(User, on_delete=models.CASCADE)
	# username = models.CharField(max_length=25)
	# email = models.EmailField(max_length=30)
	# password = models.CharField(max_length=25)
	# repassword = models.CharField(max_length=25)
	fullname = models.CharField(max_length=30)
	CATEGORY_GENDER = (('Male','Male'), ('Female','Female'))
	gender = models.CharField(max_length=6, choices=CATEGORY_GENDER)
	date_of_birth = models.CharField(max_length=10)
	#time_of_birth = models.CharField(max_length=10)
	birth_place = models.CharField(max_length=25)
	marital_status = models.ForeignKey(MaritalStatus, on_delete=models.CASCADE, related_name='personal_maritalstatus')
	no_of_childrens = models.IntegerField(default=0)
	mother_tongue = models.ForeignKey(MotherTongue, on_delete=models.CASCADE, related_name='personal_mothertongue')
	religion = models.ForeignKey(Religion, on_delete=models.CASCADE, related_name='personal_religion')
	caste = models.ForeignKey(Caste, on_delete=models.CASCADE, related_name='personal_caste')
	sub_caste = models.ForeignKey(SubCaste, on_delete=models.CASCADE, related_name='personal_subcaste')
	height = models.CharField(max_length=5)
	weight = models.IntegerField()
	blood_group = models.ForeignKey(BloodGroup, on_delete=models.CASCADE)
	complexion = models.ForeignKey(Complexion, on_delete=models.CASCADE)
	body_type = models.ForeignKey(BodyType, on_delete=models.CASCADE)
	manglik = models.CharField(max_length=10)
	gothram = models.CharField(max_length=20)
	rashi_name = models.ForeignKey(StarRashi, on_delete=models.CASCADE, related_name='personal_rashiname')
	EATING_CHOICES=(('Vegetarian','Vegetarian'),('Non-Vegetarian','Non-Vegetarian'),('Eggetarian','Eggetarian'))
	eating_habits = models.CharField(max_length=20, choices=EATING_CHOICES)
	DRINKING_CHOICES = (('No','No'),('Occassionally','Occassionally'),('Yes','Yes'))
	drinking_habits = models.CharField(max_length=20, choices=DRINKING_CHOICES)
	SMOKING_CHOICES = (('No','No'),('Occassionally','Occassionally'),('Yes','Yes'))
	smoking_habits = models.CharField(max_length=20, choices=SMOKING_CHOICES)
	disability_details = models.CharField(max_length=30)
	about_myself = models.CharField(max_length=50)

	#--Address Details -- #
	state = models.ForeignKey(State, on_delete=models.CASCADE)
	district = models.ForeignKey(District, on_delete=models.CASCADE)
	city = models.ForeignKey(City, on_delete=models.CASCADE)
	current_address = models.CharField(max_length=50)
	pincode = models.CharField(max_length=7)
	phone_number = models.CharField(max_length=12)
	mobile_number = models.CharField(max_length=10)
	mobile_number2 = models.CharField(max_length=10)
	state_ancestral = models.ForeignKey(State, on_delete=models.CASCADE, related_name='state', null=True, blank=True)
	district_ancestral = models.ForeignKey(District, on_delete=models.CASCADE, related_name='district', null=True, blank=True)
	city_ancestral = models.ForeignKey(City, on_delete=models.CASCADE, related_name='city', null=True, blank=True)
	current_address_ancestral = models.CharField(max_length=50, null=True, blank=True)
	country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, blank=True)
	nri_address = models.CharField(max_length=50, blank=True)

	#--Professional Details --#
		#--Educational Details --#
	education = models.ForeignKey(Education, on_delete=models.CASCADE, related_name='personal_education')
	qualification = models.ForeignKey(Qualification, on_delete=models.CASCADE)
	more_information = models.CharField(max_length=30)
		#--Occupational Details --#
	occupation = models.ForeignKey(Occupation, on_delete=models.CASCADE, related_name='personal_occupations')
	sub_occupation = models.ForeignKey(SubOccupation, on_delete=models.CASCADE)
	employed_in = models.CharField(max_length=30)
	annual_income = models.CharField(max_length=8)

	#--Family Background --#
	CATEGORY_FAMILY_VALUE = (('Liberal','Liberal'),('Moderate', 'Moderate'),('Orthodox','Orthodox'),('Traditional','Traditional'))
	family_value = models.CharField(max_length=20, choices = CATEGORY_FAMILY_VALUE)
	CATEGORY_FAMILY_TYPE = (('Joint','Joint'),('Nuclear','Nuclear'))
	family_type = models.CharField(max_length=20, choices = CATEGORY_FAMILY_TYPE)
	CATEGORY_FAMILY_STATUS = (('Affluent','Affluent'),('Middle Class','Middle Class'),('Rich','Rich'),('Upper Middle Class','Upper Middle Class'))
	family_status = models.CharField(max_length=20, choices=CATEGORY_FAMILY_STATUS)
	fathers_name = models.CharField(max_length=30)
	fathers_occupation = models.CharField(max_length=30)
	mothers_name = models.CharField(max_length=30)
	mothers_occupation = models.CharField(max_length=30)
	no_of_brothers = models.IntegerField()
	brother_married = models.CharField(max_length=4)
	no_of_sisters = models.IntegerField()
	sister_married = models.CharField(max_length=4)
	surname_of_relatives = models.CharField(max_length=50)
	parental_details = models.CharField(max_length=50)

	#--Partners Basic Information--#
	partners_age_from = models.IntegerField()
	partners_age_to = models.IntegerField()
	partners_height_from = models.CharField(max_length=5)
	partners_height_to = models.CharField(max_length=5)
	partners_marital_status = models.ForeignKey(MaritalStatus, on_delete=models.CASCADE)
	partners_mother_tongue = models.ForeignKey(MotherTongue, on_delete=models.CASCADE)
	#physical_disability
	PRT_EATING_CHOICES=(('Vegetarian','Vegetarian'),('Non-Vegetarian','Non-Vegetarian'),('Eggetarian','Eggetarian'))
	prt_eating_habits = models.CharField(max_length=20, choices=EATING_CHOICES)
	PRT_DRINKING_CHOICES = (('No','No'),('Occassionally','Occassionally'),('Yes','Yes'))
	prt_drinking_habits = models.CharField(max_length=20, choices=DRINKING_CHOICES)
	PRT_SMOKING_CHOICES = (('No','No'),('Occassionally','Occassionally'),('Yes','Yes'))
	prt_smoking_habits = models.CharField(max_length=20, choices=SMOKING_CHOICES)
	have_childs = models.CharField(max_length=4)
	about_partner = models.CharField(max_length=30)

	#--Partners Religious Information --#
	partners_religion = models.ForeignKey(Religion, on_delete=models.CASCADE)
	partners_caste = models.ForeignKey(Caste, on_delete=models.CASCADE)
	partners_sub_caste = models.ForeignKey(SubCaste, on_delete=models.CASCADE)
	partners_rashi_name = models.ForeignKey(StarRashi, on_delete=models.CASCADE)
	partners_manglik = models.CharField(max_length=5)

	#--Partners Professional Details --#
	partners_education = models.ForeignKey(Education, on_delete=models.CASCADE)
	partners_occupation = models.ForeignKey(Occupation, on_delete=models.CASCADE)
	annual_income_from = models.CharField(max_length=8)
	annual_income_to = models.CharField(max_length=8)
	#--uploads--#
	photo1 = models.ImageField(upload_to='images/',null=True,blank=True)
	photo2 = models.ImageField(upload_to='images/',null=True,blank=True)
	photo3 = models.ImageField(upload_to='images/',null=True,blank=True)

	#--Identity Proof Upload --#
	adhaar_card_number = models.CharField(max_length=13)
	adhaar_card_photocopy = models.ImageField(upload_to='images/',null=True,blank=True)
	def __str__(self):
		return self.fullname



class ResetPassword(models.Model):
	pass_id=models.AutoField(primary_key=True)
	email_address=models.EmailField(max_length=30)
	activation_id=models.CharField(max_length=50)
	client_ip=models.CharField(max_length=50)
	agent=models.CharField(max_length=50)
	created_dtm=models.DateField(default=timezone.now)
	