from memebership.models import MembershipPlan
from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(MembershipPlan)