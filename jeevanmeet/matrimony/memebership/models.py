from django.db import models

# Create your models here.
class MembershipPlan(models.Model):
	plan_id = models.AutoField(primary_key=True)
	plan_name = models.CharField(max_length=30)
	amount = models.IntegerField()
	days = models.CharField(max_length=20)
	profiles = models.CharField(max_length=20)

	def __str__(self):
		return str(self.plan_name)

 

