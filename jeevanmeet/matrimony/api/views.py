from core.models import MembershipPlan
from core.models import Userinfo
from django.http.response import Http404
from django.shortcuts import render
from rest_framework.views import APIView
from .serializers import *
from rest_framework.response import Response
from rest_framework import status


from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
# Create your views here.


from django.views.generic import CreateView,DetailView,DeleteView,UpdateView,ListView
#from .forms import *
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        #token['name'] = user.first_name
        return token

class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer



class RegisterAPI(APIView):

# POST API
    def post(self, request, format=None):
        data = request.data
        serializer = RegisterSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BridesAPI(APIView):
    def get(self,request,format=None):
        register = PersonalDetails.objects.filter(gender = 'Female')
        serializer = BridesSerializer(register,many=True)
        return Response(serializer.data)



class ASearchAPI(generics.ListCreateAPIView):
  
    search_fields = ['gender','complexion','marital_status','mother_tongue','religion','caste','sub_caste','education','body_type']
    filter_backends = (filters.SearchFilter,)
    queryset = PersonalDetails.objects.all()
    serializer_class = RegisterSerializer

class IdSearchAPI(generics.ListCreateAPIView):
  
    search_fields = ['user_id']
    filter_backends = (filters.SearchFilter,)
    queryset = PersonalDetails.objects.all()
    serializer_class = RegisterSerializer

class SearchAPI(APIView):
    def get(self,request,format=None):
        gender = self.request.query_params.get('gender', None)
        complexion = self.request.query_params.get('complexion', None)
        marital_status = self.request.query_params.get('marital_status', None)
        mother_tongue = self.request.query_params.get('mother_tongue', None)
        religion = self.request.query_params.get('religion', None)
        caste = self.request.query_params.get('caste', None)
        sub_caste = self.request.query_params.get('sub_caste', None)
        education = self.request.query_params.get('education', None)
        body_type = self.request.query_params.get('body_type', None)
        args = {}
        if gender:
            args['gender'] = gender
        if complexion:
            args['complexion'] = complexion
        if marital_status:
            args['marital_status'] = marital_status
        if mother_tongue:
            args['mother_tongue'] = mother_tongue
        if religion:
            args['religion'] = religion
        if caste:
            args['caste'] = caste
        if sub_caste:
            args['sub_caste'] = sub_caste
        if education:
            args['education'] = education
        if body_type:
            args['body_type'] = body_type

        register = PersonalDetails.objects.filter(**args).order_by('-user_id')
        serializer = BridesSerializer(register,many=True)
        return Response(serializer.data)




class DetailBridesGetAPI(APIView):

    def get_object(self, pk):
        try:
            return PersonalDetails.objects.get(pk=pk)
        except PersonalDetails.DoesNotExist:
            return Response(status=404)

    def get(self, request, pk, format=None):
        try:
            brides_id = PersonalDetails.objects.get(pk=pk)
            serializer = PersonalDetailsSerializer(brides_id)
            return Response(serializer.data)
        except PersonalDetails.DoesNotExist:
            return Response(status=404)                                     

class GroomAPI(APIView):
    def get(self,request,format=None):
        register = PersonalDetails.objects.filter(gender = 'Male')
        serializer = GroomSerializer(register,many=True)
        return Response(serializer.data)


class DetailGroomsGetAPI(APIView):

    def get_object(self, pk):
        try:
            return PersonalDetails.objects.get(pk=pk)
        except PersonalDetails.DoesNotExist:
            return Response(status=404)

    def get(self, request, pk, format=None):
        try:
            grooms_id = PersonalDetails.objects.get(pk=pk)
            serializer = PersonalDetailsSerializer(grooms_id)
            return Response(serializer.data)
        except PersonalDetails.DoesNotExist:
            return Response(status=404)

class ProfileData(APIView):
    def get(self,request,format=None):
        pd = PersonalDetails.objects.all()
        serializer = ProfileDataSerializer(pd,many=True)
        return Response(serializer.data)

class EditProfile(APIView):
    def get_object(self,pk):
        try:
            return PersonalDetails.objects.get(pk=pk)
        except PersonalDetails.DoesNotExist:
                raise Http404


    def put(self, request,pk,format=None):
        user_id = self.get_object(pk)
        serializer = EditProfileSerializer(user_id, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DemoAPI(APIView):

# POST API
    def post(self, request, format=None):
        data = request.data
        serializer = DemoSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# GET API
    def get(self,request,format=None):
        register = Demo.objects.all()
        serializer = DemoSerializer(register,many=True)
        return Response(serializer.data)


class DemoUpdate(APIView):
    def get_object(self,pk):
        try:
            return Demo.objects.get(pk=pk)
        except Demo.DoesNotExist:
                raise Http404


    def put(self, request,pk,format=None):
        demo_id = self.get_object(pk)
        serializer = DemoSerializer(demo_id, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




class DemoDeleteAPI(APIView):
    def get_object(self, pk):
        try:
            return Demo.objects.get(pk=pk)
        except Demo.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        demo_id = self.get_object(pk)
        demo_id.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



class DetailDemoGetAPI(APIView):
    def get_object(self, pk):
        try:
            return Demo.objects.get(pk=pk)
        except Demo.DoesNotExist:
            return Response(status=404)
            
    def get(self, request, pk, format=None):
        try:
            demo_id = Demo.objects.get(pk=pk)
            serializer = DemoSerializer(demo_id)
            return Response(serializer.data)
        except Demo.DoesNotExist:
            return Response(status=404)

class FeedbackAPI(APIView):

# POST API
    def post(self, request, format=None):
        data = request.data
        serializer = FeedbackSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MaritalStatusAPI(APIView):

# GET API
    def get(self,request,format=None):
        ms = MaritalStatus.objects.all()
        serializer = MaritalStatusSerializer(ms,many=True)
        return Response(serializer.data)

class MotherTongueAPI(APIView):

# GET API
    def get(self,request,format=None):
        mt = MotherTongue.objects.all()
        serializer = MotherTongueSerializer(mt,many=True)
        return Response(serializer.data)

class ReligionAPI(APIView):

# GET API
    def get(self,request,format=None):
        rg = Religion.objects.all()
        serializer = ReligionSerializer(rg,many=True)
        return Response(serializer.data)


class CasteAPI(APIView):

# GET API
    def get(self,request,format=None):
        ct = Caste.objects.all()
        serializer = CasteSerializer(ct,many=True)
        return Response(serializer.data)

class SubCasteAPI(APIView):

# GET API
    def get(self,request,format=None):
        sct = SubCaste.objects.all()
        serializer = SubCasteSerializer(sct,many=True)
        return Response(serializer.data)

class BloodGroupAPI(APIView):

# GET API
    def get(self,request,format=None):
        bg = BloodGroup.objects.all()
        serializer = BloodGroupSerializer(bg,many=True)
        return Response(serializer.data)

class ComplexionAPI(APIView):

# GET API
    def get(self,request,format=None):
        cx = Complexion.objects.all()
        serializer = ComplexionSerializer(cx,many=True)
        return Response(serializer.data)

class BodyTypeAPI(APIView):

# GET API
    def get(self,request,format=None):
        bt = BodyType.objects.all()
        serializer = BodyTypeSerializer(bt,many=True)
        return Response(serializer.data)

#class ManglikAPI(APIView):

# GET API
    #def get(self,request,format=None):
        #ml = Manglik.objects.all()
        #serializer = ManglikSerializer(ml,many=True)
        #return Response(serializer.data)

class RashiAPI(APIView):

# GET API
    def get(self,request,format=None):
        rn = StarRashi.objects.all()
        serializer = StarRashiSerializer(rn,many=True)
        return Response(serializer.data)

class StateAPI(APIView):

# GET API
    def get(self,request,format=None):
        st = State.objects.all()
        serializer = StateSerializer(st,many=True)
        return Response(serializer.data)

class DistrictAPI(APIView):

# GET API
    def get(self,request,format=None):
        dt= District.objects.all()
        serializer = DistrictSerializer(dt,many=True)
        return Response(serializer.data)

class CityAPI(APIView):

# GET API
    def get(self,request,format=None):
        ct = City.objects.all()
        serializer = CitySerializer(ct,many=True)
        return Response(serializer.data)

class StateancAPI(APIView):

# GET API
    def get(self,request,format=None):
        st = State.objects.all()
        serializer = StateSerializer(st,many=True)
        return Response(serializer.data)

class DistrictancAPI(APIView):

# GET API
    def get(self,request,format=None):
        dt= District.objects.all()
        serializer = DistrictSerializer(dt,many=True)
        return Response(serializer.data)

class CityancAPI(APIView):

# GET API
    def get(self,request,format=None):
        ct = City.objects.all()
        serializer = CitySerializer(ct,many=True)
        return Response(serializer.data)

class CountryAPI(APIView):

# GET API
    def get(self,request,format=None):
        cty = Country.objects.all()
        serializer = CountrySerializer(cty,many=True)
        return Response(serializer.data)
        
class CountryAPI(APIView):

# GET API
    def get(self,request,format=None):
        cty = Country.objects.all()
        serializer = CountrySerializer(cty,many=True)
        return Response(serializer.data)

class CountryAPI(APIView):

# GET API
    def get(self,request,format=None):
        cty = Country.objects.all()
        serializer = CountrySerializer(cty,many=True)
        return Response(serializer.data)

class EducationAPI(APIView):

# GET API
    def get(self,request,format=None):
        edu = Education.objects.all()
        serializer = EducationSerializer(edu,many=True)
        return Response(serializer.data)

class QualificationAPI(APIView):

# GET API
    def get(self,request,format=None):
        qua =  Qualification.objects.all()
        serializer =  QualificationSerializer(qua,many=True)
        return Response(serializer.data)

class OccupationAPI(APIView):
      
# GET API
    def get(self,request,format=None):
        occ = Occupation.objects.all()
        serializer = OccupationSerializer(occ,many=True)
        return Response(serializer.data)

class SubOccupationAPI(APIView):
      
# GET API
    def get(self,request,format=None):
        subocc = SubOccupation.objects.all()
        serializer = SubOccupationSerializer(subocc,many=True)
        return Response(serializer.data)
        

class PReligionAPI(APIView):

# GET API
    def get(self,request,format=None):
        prg = Religion.objects.all()
        serializer = PReligionSerializer(prg,many=True)
        return Response(serializer.data)


class PCasteAPI(APIView):

# GET API
    def get(self,request,format=None):
        pct = Caste.objects.all()
        serializer = PCasteSerializer(pct,many=True)
        return Response(serializer.data)

class PSubCasteAPI(APIView):

# GET API
    def get(self,request,format=None):
        psct = SubCaste.objects.all()
        serializer = PSubCasteSerializer(psct,many=True)
        return Response(serializer.data)

class PEducationAPI(APIView):

# GET API
    def get(self,request,format=None):
        pedu = Education.objects.all()
        serializer = PEducationSerializer(pedu,many=True)
        return Response(serializer.data)

class POccupationAPI(APIView):
      
# GET API
    def get(self,request,format=None):
        pocc = Occupation.objects.all()
        serializer = POccupationSerializer(pocc,many=True)
        return Response(serializer.data)

class PMaritalStatusAPI(APIView):

# GET API
    def get(self,request,format=None):
        pms = MaritalStatus.objects.all()
        serializer = PMaritalStatusSerializer(pms,many=True)
        return Response(serializer.data)

class PMotherTongueAPI(APIView):

# GET API
    def get(self,request,format=None):
        pmt = MotherTongue.objects.all()
        serializer = PMotherTongueSerializer(pmt,many=True)
        return Response(serializer.data)

class PRashiAPI(APIView):

# GET API
    def get(self,request,format=None):
        prn = StarRashi.objects.all()
        serializer = PStarRashiSerializer(prn,many=True)
        return Response(serializer.data)


class FranchiesAPI(APIView):

# POST API
    def post(self, request, format=None):
        data = request.data
        serializer = FranchiesSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FindFranchiesAPI(APIView):
    def get(self,request,format=None):
        fregister = FranchiseRegister.objects.filter()
        serializer = FranchiseRegisterSerializer(fregister,many=True)
        return Response(serializer.data)

class TellAPI(APIView):

# POST API
    def post(self, request, format=None):
         data = request.data
         serializer = TellSerializer(data=data)
         if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MemebershipPlanAPI(APIView):
    def get(self,request,format=None):
        mp = MembershipPlan.objects.all()
        serializer = MembershipPlanSerializer(mp,many=True)
        return Response(serializer.data)

class DetailPlanGetAPI(APIView):

    def get_object(self, pk):
        try:
            return MembershipPlan.objects.get(pk=pk)
        except MembershipPlan.DoesNotExist:
            return Response(status=404)

    def get(self, request, pk, format=None):
        try:
            plan_id = MembershipPlan.objects.get(pk=pk)
            serializer = MembershipPlanSerializer(plan_id)
            return Response(serializer.data)
        except PersonalDetails.DoesNotExist:
            return Response(status=404)                        
                               
class UpgradePlanAPI(APIView):
    def get(self,request,format=None):
        up = UpgradePlan.objects.all()
        serializer = UpgradePlanSerializer(up,many=True)
        return Response(serializer.data)


class UserAPI(APIView):

    def get_object(self, pk):
        try:
            return Userinfo.objects.get(pk=pk)
        except Userinfo.DoesNotExist:
            return Response(status=404)

    def get(self, request, pk, format=None):
        try:
            user_id = Userinfo.objects.get(pk=pk)
            serializer = UserinfoSerializer(user_id)
            return Response(serializer.data)
        except PersonalDetails.DoesNotExist:
            return Response(status=404)    