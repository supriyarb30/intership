from django.urls import path,include
#from rest_framework import routers
from api.views import *
from api import views
from rest_framework_simplejwt import views as jwt_views



urlpatterns = [
	            path('register/',RegisterAPI.as_view(),name='register'),
                path('brides/',BridesAPI.as_view(),name='brides'),
				path('detail-bridesget/<int:pk>',DetailBridesGetAPI.as_view(),name="detail-bridesget"),
				path('grooms/',GroomAPI.as_view(),name='groom'),
				path('detail-groomsget/<int:pk>',DetailGroomsGetAPI.as_view(),name="detail-groomsget"),
				#path('regulaersearch/',SearchAPI.as_view(),name="regulaersearch"),
				path('regulaersearch/',ASearchAPI.as_view(),name="regulaersearch"),
				path('idsearch/',IdSearchAPI.as_view(),name="idsearch"),
				
				path('get-profile/<int:pk>',ProfileData.as_view(),name='get-profile'),
                path('edit-profile/<int:pk>',EditProfile.as_view(),name='edit-profile'),

			    path('token/', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
			    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),

                path('demo/',DemoAPI.as_view(),name='demo'),
				path('edit-demo/<int:pk>',DemoUpdate.as_view(),name='edit-demo'),

				#path('edit-demo/<int:pk>',DemoUpdate.as_view(),name='edit-demo'),
				path('delete-demo/<int:pk>',DemoDeleteAPI.as_view(),name='delete-demo'),
				
				path('detail-demo/<int:pk>',DetailDemoGetAPI.as_view(),name='detail-demo'),

				path('feedback/',FeedbackAPI.as_view(),name='feedback'),

				path('maritalstatus/',MaritalStatusAPI.as_view(),name='maritalstatus'),
				path('mothertongue/',MotherTongueAPI.as_view(),name='mothertongue'),
				path('religion/',ReligionAPI.as_view(),name='religion'),
				path('caste/',CasteAPI.as_view(),name='caste'),
				path('subcaste/',SubCasteAPI.as_view(),name='subcaste'),
				path('bloodgroup/',BloodGroupAPI.as_view(),name='bloodgroup'),
				path('complexion/',ComplexionAPI.as_view(),name='complexion'),
				path('bodytype/',BodyTypeAPI.as_view(),name='bodytype'),
				#path('manglik/',ManglikAPI.as_view(),name='manglik'),
				path('rashi/',RashiAPI.as_view(),name='rashi'),
				path('state/',StateAPI.as_view(),name='state'),
				path('district/',DistrictAPI.as_view(),name='district'),
				path('city/',CityAPI.as_view(),name='city'),
				path('stateanc/',StateancAPI.as_view(),name='stateanc'),
				path('districtanc/',DistrictancAPI.as_view(),name='districtanc'),
				path('cityanc/',CityancAPI.as_view(),name='cityanc'),
				path('country/',CountryAPI.as_view(),name='country'),
				path('education/',EducationAPI.as_view(),name='education'),
				path('qualification/',QualificationAPI.as_view(),name='qualification'),
				path('occupation/',OccupationAPI.as_view(),name='occupation'),
				path('suboccupation/',SubOccupationAPI.as_view(),name='suboccupation'),
				path('preligion/',PReligionAPI.as_view(),name='preligion'),
				path('pcaste/',PCasteAPI.as_view(),name='pcaste'),
				path('psubcaste/',PSubCasteAPI.as_view(),name='psubcaste'),
				path('peducation/',PEducationAPI.as_view(),name='peducation'),
				path('poccupation/',POccupationAPI.as_view(),name='poccupation'),
				path('pmaritalstatus/',PMaritalStatusAPI.as_view(),name='pmaritalstatus'),
				path('pmothertongue/',PMotherTongueAPI.as_view(),name='pmothertongue'),
				path('prashi/',PRashiAPI.as_view(),name='prashi'),
				
				
				path('franchiesregister/',FranchiesAPI.as_view(),name='franchiesregister'),
				path('findfranchies/',FindFranchiesAPI.as_view(),name='findfranchies'),
				#path('detail-bridesget/<int:pk>',DetailBridesGetAPI.as_view(),name="detail-bridesget"),
				path('tellstory/',TellAPI.as_view(),name='tellstory'),
				path('memebershipplan/',MemebershipPlanAPI.as_view(),name='memebershipplan'),
				path('detail-plan/<int:pk>',DetailPlanGetAPI.as_view(),name='detail-plan'),
				path('userinfo/<int:pk>',UserAPI.as_view(),name='userinfo'),
				#path('upgrade-plan/',UpgradePlanAPI.as_view(),name='upgrade-plan'),




				            

]