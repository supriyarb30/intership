from core.models import MembershipPlan
from core.models import Userinfo
from blog.models import TellStory
from franchise.models import FranchiseRegister
from customer.models import PersonalDetails
from rest_framework import serializers
from .models import *
import base64, uuid
from django.core.files.base import ContentFile
from rest_framework import generics
from rest_framework import filters



class Base64ImageField(serializers.ImageField):
	def to_internal_value(self, data):
		if isinstance(data, str) and data.startswith('data:image'):
			# base64 encoded image - decode
			format, imgstr = data.split(';base64,') # format ~= data:image/X,
			ext = format.split('/')[-1] # guess file extension
			id = uuid.uuid4()
			data = ContentFile(base64.b64decode(imgstr), name = id.urn[9:] + '.' + ext)
		return super(Base64ImageField, self).to_internal_value(data)




class RegisterSerializer(serializers.ModelSerializer):
	photo1 = Base64ImageField(max_length=None, use_url=True,)
	photo2 = Base64ImageField(max_length=None, use_url=True,)
	photo3 = Base64ImageField(max_length=None, use_url=True,)
	adhaar_card_photocopy = Base64ImageField(max_length=None, use_url=True,)
	marital_status=serializers.CharField(source='marital_status.maritalstatus_name')
	mother_tongue=serializers.CharField(source='mother_tongue.mothertongue_name')
	religion=serializers.CharField(source='religion.religion_name')
	caste=serializers.CharField(source='caste.caste_name')
	sub_caste=serializers.CharField(source='sub_caste.subcaste_name')
	blood_group=serializers.CharField(source='blood_group.bloodgroup_name')
	complexion=serializers.CharField(source='complexion.complexion_name')
	body_type=serializers.CharField(source='body_type.bodytype_name')
	rashi_name=serializers.CharField(source='rashi_name.rashi_name')
	state=serializers.CharField(source='state.state_name')
	district=serializers.CharField(source='district.district_name')
	city=serializers.CharField(source='city.city_name')
	state_ancestral=serializers.CharField(source='state_ancestral.state_name')
	district_ancestral=serializers.CharField(source='district_ancestral.district_name')
	city_ancestral=serializers.CharField(source='city_ancestral.city_name')
	country=serializers.CharField(source='country.country_name')
	education=serializers.CharField(source='education.education_name')
	qualification=serializers.CharField(source='qualification.qualification_name')
	occupation=serializers.CharField(source='occupation.occupation_name')
	sub_occupation=serializers.CharField(source='sub_occupation.suboccupation_name')
	partners_marital_status=serializers.CharField(source='partners_marital_status.maritalstatus_name')
	partners_religion=serializers.CharField(source='partners_religion.religion_name')
	partners_caste=serializers.CharField(source='partners_caste.caste_name')
	partners_sub_caste=serializers.CharField(source='partners_sub_caste.subcaste_name')
	partners_rashi_name=serializers.CharField(source='partners_rashi_name.rashi_name')
	partners_education=serializers.CharField(source='partners_education.education_name')
	partners_occupation=serializers.CharField(source='partners_occupation.occupation_name')

	class Meta:
		model = PersonalDetails
		fields = ('__all__')


class BridesSerializer(serializers.ModelSerializer):

	marital_status=serializers.CharField(source='marital_status.maritalstatus_name')
	mother_tongue=serializers.CharField(source='mother_tongue.mothertongue_name')
	religion=serializers.CharField(source='religion.religion_name')
	caste=serializers.CharField(source='caste.caste_name')
	sub_caste=serializers.CharField(source='sub_caste.subcaste_name')
	blood_group=serializers.CharField(source='blood_group.bloodgroup_name')
	complexion=serializers.CharField(source='complexion.complexion_name')
	body_type=serializers.CharField(source='body_type.bodytype_name')
	rashi_name=serializers.CharField(source='rashi_name.rashi_name')
	state=serializers.CharField(source='state.state_name')
	district=serializers.CharField(source='district.district_name')
	city=serializers.CharField(source='city.city_name')
	state_ancestral=serializers.CharField(source='state_ancestral.state_name')
	district_ancestral=serializers.CharField(source='district_ancestral.district_name')
	city_ancestral=serializers.CharField(source='city_ancestral.city_name')
	country=serializers.CharField(source='country.country_name')
	education=serializers.CharField(source='education.education_name')
	qualification=serializers.CharField(source='qualification.qualification_name')
	occupation=serializers.CharField(source='occupation.occupation_name')
	sub_occupation=serializers.CharField(source='sub_occupation.suboccupation_name')
	partners_marital_status=serializers.CharField(source='partners_marital_status.maritalstatus_name')
	partners_religion=serializers.CharField(source='partners_religion.religion_name')
	partners_caste=serializers.CharField(source='partners_caste.caste_name')
	partners_sub_caste=serializers.CharField(source='partners_sub_caste.subcaste_name')
	partners_rashi_name=serializers.CharField(source='partners_rashi_name.rashi_name')
	partners_education=serializers.CharField(source='partners_education.education_name')
	partners_occupation=serializers.CharField(source='partners_occupation.occupation_name')

	class Meta:
		model = PersonalDetails
		fields ='__all__'

class GroomSerializer(serializers.ModelSerializer):
	
	marital_status=serializers.CharField(source='marital_status.maritalstatus_name')
	mother_tongue=serializers.CharField(source='mother_tongue.mothertongue_name')
	religion=serializers.CharField(source='religion.religion_name')
	caste=serializers.CharField(source='caste.caste_name')
	sub_caste=serializers.CharField(source='sub_caste.subcaste_name')
	blood_group=serializers.CharField(source='blood_group.bloodgroup_name')
	complexion=serializers.CharField(source='complexion.complexion_name')
	body_type=serializers.CharField(source='body_type.bodytype_name')
	rashi_name=serializers.CharField(source='rashi_name.rashi_name')
	state=serializers.CharField(source='state.state_name')
	district=serializers.CharField(source='district.district_name')
	city=serializers.CharField(source='city.city_name')
	state_ancestral=serializers.CharField(source='state_ancestral.state_name')
	district_ancestral=serializers.CharField(source='district_ancestral.district_name')
	city_ancestral=serializers.CharField(source='city_ancestral.city_name')
	country=serializers.CharField(source='country.country_name')
	education=serializers.CharField(source='education.education_name')
	qualification=serializers.CharField(source='qualification.qualification_name')
	occupation=serializers.CharField(source='occupation.occupation_name')
	sub_occupation=serializers.CharField(source='sub_occupation.suboccupation_name')
	partners_marital_status=serializers.CharField(source='partners_marital_status.maritalstatus_name')
	partners_religion=serializers.CharField(source='partners_religion.religion_name')
	partners_caste=serializers.CharField(source='partners_caste.caste_name')
	partners_sub_caste=serializers.CharField(source='partners_sub_caste.subcaste_name')
	partners_rashi_name=serializers.CharField(source='partners_rashi_name.rashi_name')
	partners_education=serializers.CharField(source='partners_education.education_name')
	partners_occupation=serializers.CharField(source='partners_occupation.occupation_name')
	class Meta:
		model = PersonalDetails
		fields ='__all__'


class PersonalDetailsSerializer(serializers.ModelSerializer):
	marital_status=serializers.CharField(source='marital_status.maritalstatus_name')
	mother_tongue=serializers.CharField(source='mother_tongue.mothertongue_name')
	religion=serializers.CharField(source='religion.religion_name')
	caste=serializers.CharField(source='caste.caste_name')
	sub_caste=serializers.CharField(source='sub_caste.subcaste_name')
	blood_group=serializers.CharField(source='blood_group.bloodgroup_name')
	complexion=serializers.CharField(source='complexion.complexion_name')
	body_type=serializers.CharField(source='body_type.bodytype_name')
	rashi_name=serializers.CharField(source='rashi_name.rashi_name')
	state=serializers.CharField(source='state.state_name')
	district=serializers.CharField(source='district.district_name')
	city=serializers.CharField(source='city.city_name')
	state_ancestral=serializers.CharField(source='state_ancestral.state_name')
	district_ancestral=serializers.CharField(source='district_ancestral.district_name')
	city_ancestral=serializers.CharField(source='city_ancestral.city_name')
	country=serializers.CharField(source='country.country_name')
	education=serializers.CharField(source='education.education_name')
	qualification=serializers.CharField(source='qualification.qualification_name')
	occupation=serializers.CharField(source='occupation.occupation_name')
	sub_occupation=serializers.CharField(source='sub_occupation.suboccupation_name')
	partners_marital_status=serializers.CharField(source='partners_marital_status.maritalstatus_name')
	partners_religion=serializers.CharField(source='partners_religion.religion_name')
	partners_caste=serializers.CharField(source='partners_caste.caste_name')
	partners_sub_caste=serializers.CharField(source='partners_sub_caste.subcaste_name')
	partners_rashi_name=serializers.CharField(source='partners_rashi_name.rashi_name')
	partners_education=serializers.CharField(source='partners_education.education_name')
	partners_occupation=serializers.CharField(source='partners_occupation.occupation_name')
	class Meta:
		model = PersonalDetails
		fields = '__all__'

class ProfileDataSerializer(serializers.ModelSerializer):
	class Meta:
		model = PersonalDetails
		fields = ('user_id','fullname','date_of_birth','birth_place','marital_status','no_of_childrens','mother_tongue','height','weight','blood_group','complexion','eating_habits','drinking_habits','smoking_habits','disability_details','about_myself'),

class EditProfileSerializer(serializers.ModelSerializer):
	class Meta:
		model = PersonalDetails
		fields = ('user_id','fullname','date_of_birth','birth_place','marital_status','no_of_childrens','mother_tongue','height','weight','blood_group','complexion','eating_habits','drinking_habits','smoking_habits','disability_details','about_myself'),

class DemoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Demo
		fields = ('demo_id','name','address','mobile_no')

class FeedbackSerializer(serializers.ModelSerializer):
	class Meta:
		model = Feedback
		fields = ('feedback_name','email','address','mobile','subject','suggestions')
		
class MaritalStatusSerializer(serializers.ModelSerializer):
	class Meta:
		model = MaritalStatus
		fields = ('maritalstatus_id','maritalstatus_name')


class MotherTongueSerializer(serializers.ModelSerializer):
	class Meta:
		model = MotherTongue
		fields = ('mothertongue_id','mothertongue_name')

class ReligionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Religion
		fields = ('religion_id','religion_name')

class CasteSerializer(serializers.ModelSerializer):
	class Meta:
		model = Caste
		fields = ('caste_id','caste_name','religion_id')

class SubCasteSerializer(serializers.ModelSerializer):
	class Meta:
		model = SubCaste
		fields = ('subcaste_id','subcaste_name')

class BloodGroupSerializer(serializers.ModelSerializer):
	class Meta:
		model = BloodGroup
		fields = ('bloodgroup_id','bloodgroup_name')

class ComplexionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Complexion
		fields = ('complexion_id','complexion_name')

class BodyTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = BodyType
		fields = ('bodytype_id','bodytype_name')

#class ManglikSerializer(serializers.ModelSerializer):
	#class Meta:
		#model = Manglik
		#fields = ('manglik_id','manglik_name')

class StarRashiSerializer(serializers.ModelSerializer):
	class Meta:
		model = StarRashi
		fields = ('rashi_id','rashi_name')

class StateSerializer(serializers.ModelSerializer):
	class Meta:
		model = State
		fields = ('state_id','state_name')

class DistrictSerializer(serializers.ModelSerializer):
	class Meta:
		model = District
		fields = ('district_id','district_name')

class CitySerializer(serializers.ModelSerializer):
	class Meta:
		model = City
		fields = ('city_id','city_name')

class StateancSerializer(serializers.ModelSerializer):
	class Meta:
		model = State
		fields = ('state_id','state_name')

class DistrictancSerializer(serializers.ModelSerializer):
	class Meta:
		model = District
		fields = ('district_id','district_name')

class CityancSerializer(serializers.ModelSerializer):
	class Meta:
		model = City
		fields = ('city_id','city_name')

class CountrySerializer(serializers.ModelSerializer):
	class Meta:
		model = Country
		fields = ('country_id','country_name')

class EducationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Education
		fields = ('education_id','education_name')


class QualificationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Qualification
		fields = ('qualification_id','qualification_name')


class OccupationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Occupation
		fields = ('occupation_id','occupation_name')


class SubOccupationSerializer(serializers.ModelSerializer):
	class Meta:
		model = SubOccupation
		fields = ('suboccupation_id','suboccupation_name')

class PReligionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Religion
		fields = ('religion_id','religion_name')

class PCasteSerializer(serializers.ModelSerializer):
	class Meta:
		model = Caste
		fields = ('caste_id','caste_name')

class PSubCasteSerializer(serializers.ModelSerializer):
	class Meta:
		model = SubCaste
		fields = ('subcaste_id','subcaste_name')

class PEducationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Education
		fields = ('education_id','education_name')



class POccupationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Occupation
		fields = ('occupation_id','occupation_name')

class PMaritalStatusSerializer(serializers.ModelSerializer):
	class Meta:
		model = MaritalStatus
		fields = ('maritalstatus_id','maritalstatus_name')


class PMotherTongueSerializer(serializers.ModelSerializer):
	class Meta:
		model = MotherTongue
		fields = ('mothertongue_id','mothertongue_name')

class PStarRashiSerializer(serializers.ModelSerializer):
	class Meta:
		model = StarRashi
		fields = ('rashi_id','rashi_name')


class FranchiesSerializer(serializers.ModelSerializer):
	images = Base64ImageField(max_length=None, use_url=True,)
	

	class Meta:
		model = FranchiseRegister
		fields = '__all__'


class FranchiseRegisterSerializer(serializers.ModelSerializer):
	class Meta:
		model = FranchiseRegister
		fields ='__all__'

class TellSerializer(serializers.ModelSerializer):
	photo = Base64ImageField(max_length=None, use_url=True,)
	class Meta:
 		model = TellStory
 		fields = '__all__'


class MembershipPlanSerializer(serializers.ModelSerializer):
	class Meta:
 		model = MembershipPlan
 		fields = '__all__'

class MembershipPlanSerializer(serializers.ModelSerializer):
	class Meta:
 		model = MembershipPlan
 		fields = '__all__'
	
class UpgradePlanSerializer(serializers.ModelSerializer):
	class Meta:
 		model = UpgradePlan
 		fields = '__all__'

class UserinfoSerializer(serializers.ModelSerializer):
	class Meta:
 		model = Userinfo
 		fields = '__all__'


