from django.shortcuts import render
from .models import *
from django.views.generic import TemplateView, DetailView,CreateView,UpdateView,DetailView,ListView
from .blog_search import *
from .tellstory_search import *
from django.urls import reverse
from .forms import *
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect

# Create your views here.

class BlogList(ListView):
	model = Blog
	paginate_by = 10
	template_name = 'blog/blog_list.html'
	form_class = BlogSearchForm

	def get_context_data(self, **kwargs):
		context = super(BlogList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] = BlogSearchForm(self.request.GET)
		else:
			context['form'] = BlogSearchForm()
		return context

	def get_queryset(self):
		args = {}
		blog_id = self.request.GET.get('blog_id',False)
		entry_title = self.request.GET.get('entry_title',False)
		entry_body = self.request.GET.get('entry_body', False)
		blog_date = self.request.GET.get('blog_date', False)
		status = self.request.GET.get('status', False)
		slug = self.request.GET.get('slug', False)

		if blog_id:
			args['blog_id__icontains'] = blog_id

		if entry_title:
			args['entry_title__icontains'] = entry_title

		if entry_body:
			args['entry_body__icontains'] = entry_body

		if blog_date:
			args['blog_date'] = blog_date
	
		if status:
			args['status'] = status

		if slug:
			args['slug'] = slug

		return Blog.objects.filter(**args).order_by('-blog_id')

class BlogCreate(CreateView):
	model = Blog
	form_class = BlogForm
	template_name = "blog/blog_form.html"
	success_url = reverse_lazy('blog')
	

	def get_context_data(self, **kwargs):
		context = super(BlogCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 


class BlogUpdate(UpdateView):
	model = Blog
	form_class = BlogForm
	template_name="blog/blog_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('blog')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRBlogedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class BlogDetail(DetailView):
	model = Blog
	template_name = "blog/blog_detail.html"


class StoryList(ListView):
	model = TellStory
	paginate_by = 10
	template_name = 'blog/story_list.html'
	form_class = StorySearchForm

	def get_context_data(self, **kwargs):
		context = super(StoryList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] = StorySearchForm(self.request.GET)
		else:
			context['form'] = StorySearchForm()
		return context

	def get_queryset(self):
		args = {}
		your_name = self.request.GET.get('your_name',False)
		partner_name = self.request.GET.get('partner_name',False)
		wedding_date = self.request.GET.get('wedding_date', False)
		photo = self.request.GET.get('photo', False)
		

		if your_name:
			args['your_name__icontains'] = your_name

		if partner_name:
			args['partner_name__icontains'] = partner_name

		if wedding_date:
			args['wedding_date'] = wedding_date

		if photo:
			args['photo__icontains'] = photo
	
	
		return TellStory.objects.filter(**args).order_by('-your_name')