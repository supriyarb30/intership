from django.urls import path
from .views import *
from .import views
from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
          
            path('blog/',BlogList.as_view(), name='blog'),
     	    path('add-blog/', BlogCreate.as_view(), name='add-blog'),
     	    path('edit-blog/<int:pk>', BlogUpdate.as_view(), name='edit-blog'),
     	    path('detail-blog/<int:pk>', BlogDetail.as_view(), name='detail-blog'),
			
			path('tellstory/',StoryList.as_view(), name='tellstory'),
     	   # path('add-story/', BlogCreate.as_view(), name='add-blog'),
     	   # path('edit-story/<int:pk>', BlogUpdate.as_view(), name='edit-blog'),
     	   # path('detail-story/<int:pk>', BlogDetail.as_view(), name='detail-blog'),


     	]