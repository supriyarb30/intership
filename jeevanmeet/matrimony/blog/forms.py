from django.forms import ModelForm
from django import forms
from .models import *
from .forms import *


class BlogForm(ModelForm):
    class Meta:
        model = Blog
        fields = ('blog_id','entry_title','entry_body','blog_date','status','slug')
        # exclude = ('person')


class StoryForm(ModelForm):
    class Meta:
        model = TellStory
        fields = ('tell_id','your_name','partner_name','wedding_date','story','photo')
        # exclude = ('person')
