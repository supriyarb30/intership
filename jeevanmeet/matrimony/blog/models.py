from django.db import models
from django.utils import timezone

# Create your models here.

class Blog(models.Model):
	blog_id=models.AutoField(primary_key=True)
	entry_title= models.CharField(max_length=30)
	entry_body=models.CharField(max_length=20)
	blog_date=models.DateField(default=timezone.now)
	status=models.CharField(max_length=10)
	slug=models.CharField(max_length=50)
	def __str__(self):
		return self.entry_title

class TellStory(models.Model):
	tell_id=models.AutoField(primary_key=True)
	your_name = models.CharField(max_length=20)
	partner_name = models.CharField(max_length=20)
	wedding_date = models.CharField(max_length=20)
	story = models.CharField(max_length=50)
	photo = models.ImageField(upload_to='images/',null=True,blank=True)
	def __str__(self):
		return self.your_name