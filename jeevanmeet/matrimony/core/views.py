from django.shortcuts import render
from django.views.generic import TemplateView,ListView,CreateView,UpdateView,DetailView
from .models import *
from .search_forms import *
from django.urls import reverse
from .forms import *
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect


# Create your views here.
class Home(TemplateView):
    template_name = 'projectapp1/adminhome.html'


class SampleList(ListView):
	model = Sample
	paginate_by = 10
	template_name = "projectapp1/sample_list.html"
	form_class = SampleSearchForm

	def get_context_data(self, **kwargs):
		context = super(SampleList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] = SampleSearchForm(self.request.GET)
		else:
			context['form'] = SampleSearchForm()
		return context

	def get_queryset(self):
		args = {}
		code = self.request.GET.get('code',False)
		full_name_ma = self.request.GET.get('full_name_ma',False)
		full_name_en = self.request.GET.get('full_name_en', False)
		village_en = self.request.GET.get('village_en', False)
		taluka_en = self.request.GET.get('taluka_en', False)
		dist_en = self.request.GET.get('dist_en', False)

		if code:
			args['code__icontains'] = code

		if full_name_ma:
			args['full_name_ma__icontains'] = full_name_ma

		if full_name_en:
			args['full_name_en__icontains'] = full_name_en

		if village_en:
			args['village_en'] = village_en
	
		if taluka_en:
			args['taluka_en'] = taluka_en

		if dist_en:
			args['dist_en'] = dist_en

		return Sample.objects.filter(**args).order_by('-sr_no')

class SampleCreate(CreateView):
	model = Sample
	form_class = SampleForm
	template_name = "projectapp1/sample_form.html"
	success_url = reverse_lazy('sample')
	

	def get_context_data(self, **kwargs):
		context = super(SampleCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 


class SampleUpdate(UpdateView):
	model = Sample
	form_class = SampleForm
	template_name="projectapp1/sample_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('sample')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class SampleDetail(DetailView):
	model = Sample
	template_name = "projectapp1/sample_detail.html"



		