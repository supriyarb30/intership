from django.urls import path
from core.views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
	    path('home/', Home.as_view(), name='home'),
	    path('sample/', SampleList.as_view(), name='sample'),
     	path('add-sample/', SampleCreate.as_view(), name='add-sample'),
     	path('edit-sample/<int:pk>', SampleUpdate.as_view(), name='edit-sample'),
     	path('detail-sample/<int:pk>', SampleDetail.as_view(), name='detail-sample'),

      


]