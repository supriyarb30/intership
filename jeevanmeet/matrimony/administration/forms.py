from django.forms import ModelForm
from django import forms
from .models import *
from .forms import *


class SampleForm(ModelForm):
    class Meta:
        model = Sample
        fields = '__all__'
        # exclude = ('person')


class CastForm(ModelForm):
    class Meta:
        model = Caste
        fields = ('caste_id','caste_name','religion_id')
        # exclude = ('person')


class ReligionForm(ModelForm):
    class Meta:
        model = Religion
        fields = ('religion_id','religion_name')
        # exclude = ('person')

class MotherTongueForm(ModelForm):
    class Meta:
        model = MotherTongue
        fields = ('mothertongue_id','mothertongue_name')
        # exclude = ('person')

class SubCasteForm(ModelForm):
    class Meta:
        model = SubCaste
        fields = ('subcaste_id','subcaste_name','caste_id')
        # exclude = ('person')

class StarRashiForm(ModelForm):
    class Meta:
        model = StarRashi
        fields = ('rashi_id','rashi_name')
        # exclude = ('person')

class MaritalStatusForm(ModelForm):
    class Meta:
        model = MaritalStatus
        fields = ('maritalstatus_id','maritalstatus_name')
        # exclude = ('person')

class ComplexionForm(ModelForm):
    class Meta:
        model = Complexion
        fields = ('complexion_id','complexion_name')
        # exclude = ('person')

class BloodGroupForm(ModelForm):
    class Meta:
        model = BloodGroup
        fields = ('bloodgroup_id','bloodgroup_name')
        # exclude = ('person')

class BodyTypeForm(ModelForm):
    class Meta:
        model = BodyType
        fields = ('bodytype_id','bodytype_name')
        # exclude = ('person')

class EducationForm(ModelForm):
    class Meta:
        model = Education
        fields = '__all__'
        # exclude = ('person')
        
class QualificationForm(ModelForm):
    class Meta:
        model = Qualification
        fields = ('qualification_id','qualification_name','education_id')
        # exclude = ('person')

class OccupationForm(ModelForm):
    class Meta:
        model = Occupation
        fields = ('occupation_id','occupation_name')
        # exclude = ('person')

class SubOccupationForm(ModelForm):
    class Meta:
        model = SubOccupation
        fields = ('suboccupation_id','suboccupation_name','occupation_id')
        # exclude = ('person')

class CountryForm(ModelForm):
    class Meta:
        model = Country
        fields = ('country_id','country_name')
        # exclude = ('person')


class StateForm(ModelForm):
    class Meta:
        model =State
        fields = ('state_id','state_name','country_id')
        # exclude = ('person')


class DistrictForm(ModelForm):
    class Meta:
        model =District
        fields = ('district_id','district_name','state_id')
        # exclude = ('person')

class CityForm(ModelForm):
    class Meta:
        model =City
        fields = ('city_id','city_name','district_id')
        # exclude = ('person')

