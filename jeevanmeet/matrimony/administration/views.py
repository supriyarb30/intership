from django.shortcuts import render
from .models import *
from django.views.generic import TemplateView, DetailView,CreateView,UpdateView,DetailView,ListView
from .search_forms import *
from django.urls import reverse
from .forms import *
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect


# Create your views here.

class AdminHome(TemplateView):
    template_name = 'administration/adminhome.html'

class SampleList(ListView):
	model = Sample
	paginate_by = 10
	template_name = 'administration/sample_list.html'
	form_class = SampleSearchForm

	def get_context_data(self, **kwargs):
		context = super(SampleList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] = SampleSearchForm(self.request.GET)
		else:
			context['form'] = SampleSearchForm()
		return context

	def get_queryset(self):
		args = {}
		code = self.request.GET.get('code',False)
		full_name_ma = self.request.GET.get('full_name_ma',False)
		full_name_en = self.request.GET.get('full_name_en', False)
		village_en = self.request.GET.get('village_en', False)
		taluka_en = self.request.GET.get('taluka_en', False)
		dist_en = self.request.GET.get('dist_en', False)

		if code:
			args['code__icontains'] = code

		if full_name_ma:
			args['full_name_ma__icontains'] = full_name_ma

		if full_name_en:
			args['full_name_en__icontains'] = full_name_en

		if village_en:
			args['village_en'] = village_en
	
		if taluka_en:
			args['taluka_en'] = taluka_en

		if dist_en:
			args['dist_en'] = dist_en

		return Sample.objects.filter(**args).order_by('-sr_no')

class SampleCreate(CreateView):
	model = Sample
	form_class = SampleForm
	template_name = "administration/sample_form.html"
	success_url = reverse_lazy('sample')
	

	def get_context_data(self, **kwargs):
		context = super(SampleCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 


class SampleUpdate(UpdateView):
	model = Sample
	form_class = SampleForm
	template_name="administration/sample_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('sample')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class SampleDetail(DetailView):
	model = Sample
	template_name = "administration/sample_detail.html"




class CastList(ListView):
	model = Caste
	paginate_by = 10
	template_name = 'administration/cast_list.html'
	form_class = CastSearchForm

	def get_context_data(self, **kwargs):
		context = super(CastList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] = CastForm(self.request.GET)
		else:
			context['form'] = CastSearchForm()
		return context

	def get_queryset(self):
		args = {}
		caste_id = self.request.GET.get('caste_id',False)
		caste_name= self.request.GET.get('caste_name',False)
		religion_id= self.request.GET.get('religion_id',False)

	    

		if caste_id:
			args['caste_id'] = caste_id
		
		if caste_name:
			args['caste_name'] = caste_name

		if religion_id:
			args['religion_id']=religion_id
			                     

		return Caste.objects.filter(**args).order_by('-caste_id')

class CastCreate(CreateView):
	model = Caste
	form_class = CastForm
	template_name = "administration/cast_form.html"
	success_url = reverse_lazy('cast')

	def get_context_data(self, **kwargs):
		context = super(CastCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 
	
	
class CastUpdate(UpdateView):
	model = Caste
	form_class = CastForm
	template_name="administration/cast_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('cast')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class CastDetail(DetailView):
	model = Caste
	template_name = "administration/cast_detail.html"




class ReligionList(ListView):
	model = Religion
	paginate_by = 10
	template_name = 'administration/religion_list.html'
	form_class = ReligionSearchForm

	def get_context_data(self, **kwargs):
		context = super(ReligionList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] = ReligionSearchForm(self.request.GET)
		else:
			context['form'] = ReligionSearchForm()
		return context

	def get_queryset(self):
		args = {}
		religion_id = self.request.GET.get('religion_id',False)
		religion_name= self.request.GET.get('religion_name',False)

		if religion_id:
			args['religion_id__icontains'] = religion_id

		if religion_name:
			args['religion_name__icontains'] = religion_name


		return 	Religion.objects.filter(**args).order_by('-religion_id')

class ReligionCreate(CreateView):
	model = Religion
	form_class = ReligionForm
	template_name = "administration/religion_form.html"
	success_url = reverse_lazy('religion')
	

	def get_context_data(self, **kwargs):
		context = super(ReligionCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class ReligionUpdate(UpdateView):
	model = Religion
	form_class = ReligionForm
	template_name="administration/religion_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('religion')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class ReligionDetail(DetailView):
	model = Religion
	template_name = "administration/religion_detail.html"




class MotherTongueList(ListView):
	model = MotherTongue
	paginate_by = 10
	template_name = 'administration/mothertongue_list.html'
	form_class = MotherTongueSearchForm

	def get_context_data(self, **kwargs):
		context = super(MotherTongueList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] = MotherTongueSearchForm(self.request.GET)
		else:
			context['form'] = MotherTongueSearchForm()
		return context

	def get_queryset(self):
		args = {}
		mothertongue_id = self.request.GET.get('mothertongue_id',False)
		mothertongue_name= self.request.GET.get('mothertongue_name',False)

		if mothertongue_id:
			args['mothertongue_id__icontains'] =mothertongue_id

		if mothertongue_name:
			args['mothertongue_name__icontains'] = mothertongue_name


		return 	MotherTongue.objects.filter(**args).order_by('-mothertongue_id')

class MotherTongueCreate(CreateView):
	model = MotherTongue
	form_class = MotherTongueForm
	template_name = "administration/mothertongue_form.html"
	success_url = reverse_lazy('mothertongue')
	

	def get_context_data(self, **kwargs):
		context = super(MotherTongueCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class MotherTongueUpdate(UpdateView):
	model = MotherTongue
	form_class = MotherTongueForm
	template_name="administration/mothertongue_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('mothertongue')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class MotherTongueDetail(DetailView):
	model = MotherTongue
	template_name = "administration/mothertongue_detail.html"



class SubCasteList(ListView):
	model = SubCaste
	paginate_by = 10
	template_name = 'administration/subcaste_list.html'
	form_class = SubCasteForm

	def get_context_data(self, **kwargs):
		context = super(SubCasteList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] = SubCasteSearchForm(self.request.GET)
		else:
			context['form'] = SubCasteSearchForm()
		return context

	def get_queryset(self):
		args = {}
		subcaste_id= self.request.GET.get('subcaste_id',False)
		subcaste_name=self.request.GET.get('subcaste_name',False)
	   

		if subcaste_id:
			args['subcaste_id__icontains'] =subcaste_id

		if subcaste_name:
			args['subcaste_name__icontains'] = subcaste_name


		return 	SubCaste.objects.filter(**args).order_by('-subcaste_id')

class SubCasteCreate(CreateView):
	model = SubCaste
	form_class = SubCasteForm
	template_name = "administration/subcaste_form.html"
	success_url = reverse_lazy('subcaste')
	

	def get_context_data(self, **kwargs):
		context = super(SubCasteCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class SubCasteUpdate(UpdateView):
	model = SubCaste
	form_class = SubCasteForm
	template_name="administration/mothertongue_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('mothertongue')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class SubCasteDetail(DetailView):
	model = SubCaste
	template_name = "administration/mothertongue_detail.html"




class StarRashiList(ListView):
	model = StarRashi
	paginate_by = 10
	template_name = 'administration/starrashi_list.html'
	form_class = StarRashiForm

	def get_context_data(self, **kwargs):
		context = super(StarRashiList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] = StarRashiSearchForm(self.request.GET)
		else:
			context['form'] = StarRashiSearchForm()
		return context

	def get_queryset(self):
		args = {}
		rashi_id= self.request.GET.get('rashi_id',False)
		rashi_name=self.request.GET.get('rashi_name',False)
	   

		if rashi_id:
			args['rashi_id__icontains'] =rashi_id

		if rashi_name:
			args['rashi_name__icontains'] = rashi_name


		return 	StarRashi.objects.filter(**args).order_by('-rashi_id')

class StarRashiCreate(CreateView):
	model =StarRashi
	form_class = StarRashiForm
	template_name = "administration/starrashi_form.html"
	success_url = reverse_lazy('starrashi')
	

	def get_context_data(self, **kwargs):
		context = super(StarRashiCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class StarRashiUpdate(UpdateView):
	model = StarRashi
	form_class = StarRashiForm
	template_name="administration/starrashi_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('starrashi')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class StarRashiDetail(DetailView):
	model = StarRashi
	template_name = "administration/starrashi_detail.html"


class MaritalStatusList(ListView):
	model = MaritalStatus
	paginate_by = 10
	template_name = 'administration/maritalstatus_list.html'
	form_class = MaritalStatusForm

	def get_context_data(self, **kwargs):
		context = super(MaritalStatusList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] = MaritalStatusSearchForm(self.request.GET)
		else:
			context['form'] = MaritalStatusSearchForm()
		return context

	def get_queryset(self):
		args = {}
		maritalstatus_id= self.request.GET.get('maritalstatus_id',False)
		maritalstatus_name=self.request.GET.get('maritalstatus_name',False)
	   

		if maritalstatus_id:
			args['maritalstatus_id__icontains'] =maritalstatus_id

		if maritalstatus_name:
			args['maritalstatus_name__icontains'] = maritalstatus_name


		return 	MaritalStatus.objects.filter(**args).order_by('-maritalstatus_id')

class MaritalStatusCreate(CreateView):
	model =MaritalStatus
	form_class = MaritalStatusForm
	template_name = "administration/maritalstatus_form.html"
	success_url = reverse_lazy('maritalstatus')
	

	def get_context_data(self, **kwargs):
		context = super(MaritalStatusCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class MaritalStatusUpdate(UpdateView):
	model = MaritalStatus
	form_class = MaritalStatusForm
	template_name="administration/maritalstatus_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('maritalstatus')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class MaritalStatusDetail(DetailView):
	model = MaritalStatus
	template_name = "administration/maritalstatus_detail.html"



class ComplexionList(ListView):
	model = Complexion
	paginate_by = 10
	template_name = 'administration/complexion_list.html'
	form_class =ComplexionForm

	def get_context_data(self, **kwargs):
		context = super(ComplexionList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] =ComplexionSearchForm(self.request.GET)
		else:
			context['form'] = ComplexionSearchForm()
		return context

	def get_queryset(self):
		args = {}
		complexion_id= self.request.GET.get('complexion_id',False)
		complexion_name=self.request.GET.get('complexion_name',False)
	   

		if complexion_id:
			args['complexion_id__icontains'] =complexion_id

		if complexion_name:
			args['complexion_name__icontains'] = complexion_name

		return 	Complexion.objects.filter(**args).order_by('-complexion_id')

class ComplexionCreate(CreateView):
	model =Complexion
	form_class = ComplexionForm
	template_name = "administration/complexion_form.html"
	success_url = reverse_lazy('complexion')
	

	def get_context_data(self, **kwargs):
		context = super(ComplexionCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class ComplexionUpdate(UpdateView):
	model = Complexion
	form_class = ComplexionForm
	template_name="administration/complexion_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('complexion')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class ComplexionDetail(DetailView):
	model = Complexion
	template_name = "administration/complexion_detail.html"



class BloodGroupList(ListView):
	model = BloodGroup
	paginate_by = 10
	template_name = 'administration/bloodgroup_list.html'
	form_class =BloodGroup

	def get_context_data(self, **kwargs):
		context = super(BloodGroupList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] =BloodGroupSearchForm(self.request.GET)
		else:
			context['form'] = BloodGroupSearchForm()
		return context

	def get_queryset(self):
		args = {}
		bloodgroup_id= self.request.GET.get('bloodgroup_id',False)
		bloodgroup_name=self.request.GET.get('bloodgroup_name',False)
	   

		if bloodgroup_id:
			args['bloodgroup_id__icontains'] =bloodgroup_id

		if bloodgroup_name:
			args['bloodgroup_name__icontains'] = bloodgroup_name

		return 	BloodGroup.objects.filter(**args).order_by('-bloodgroup_id')

class BloodGroupCreate(CreateView):
	model =BloodGroup
	form_class = BloodGroupForm
	template_name = "administration/bloodgroup_form.html"
	success_url = reverse_lazy('bloodgroup')
	

	def get_context_data(self, **kwargs):
		context = super(BloodGroupCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class BloodGroupUpdate(UpdateView):
	model = BloodGroup
	form_class = BloodGroupForm
	template_name="administration/bloodgroup_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('bloodgroup')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class BloodGroupDetail(DetailView):
	model = BloodGroup
	template_name = "administration/bloodgroup_detail.html"




class BodyTypeList(ListView):
	model = BodyType
	paginate_by = 10
	template_name = 'administration/bodytype_list.html'
	form_class =BodyType

	def get_context_data(self, **kwargs):
		context = super(BodyTypeList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] =BodyTypeSearchForm(self.request.GET)
		else:
			context['form'] =BodyTypeSearchForm()
		return context

	def get_queryset(self):
		args = {}
		bodytype_id= self.request.GET.get('bodytype_id',False)
		bodytype_name=self.request.GET.get('bodytype_name',False)
	   

		if bodytype_id:
			args['bodytype_id__icontains'] =bodytype_id

		if bodytype_name:
			args['bodytype_name__icontains'] = bodytype_name

		return 	BodyType.objects.filter(**args).order_by('-bodytype_id')

class BodyTypeCreate(CreateView):
	model = BodyType
	form_class = BodyTypeForm
	template_name = "administration/bodytypes_form.html"
	success_url = reverse_lazy('bodytype')
	

	def get_context_data(self, **kwargs):
		context = super(BodyTypeCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class BodyTypeUpdate(UpdateView):
	model = BodyType
	form_class = BodyTypeForm
	template_name="administration/bodytypes_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('bodytype')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class BodyTypeDetail(DetailView):
	model = BodyType
	template_name = "administration/bodytypes_detail.html"



class EducationList(ListView):
	model = Education
	paginate_by = 10
	template_name = 'administration/bodytype_list.html'
	form_class =Education

	def get_context_data(self, **kwargs):
		context = super(EducationList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] =EducationSearchForm(self.request.GET)
		else:
			context['form'] =EducationSearchForm()
		return context

	def get_queryset(self):
		args = {}
		education_id= self.request.GET.get('education_id',False)
		education_id=self.request.GET.get('education_id',False)
	   

		if education_id:
			args['education_id__icontains'] =education_id

		if bodytype_name:
			args['education_name__icontains'] = education_name

		return 	Education.objects.filter(**args).order_by('-education_id')

class EducationCreate(CreateView):
	model = Education
	form_class = EducationForm
	template_name = "administration/education_form.html"
	success_url = reverse_lazy('education')
	

	def get_context_data(self, **kwargs):
		context = super(EducationCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class EducationUpdate(UpdateView):
	model = Education
	form_class = EducationForm
	template_name="administration/education_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('education')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class EducationDetail(DetailView):
	model = Education
	template_name = "administration/education_detail.html"



class QualificationList(ListView):
	model = Qualification
	paginate_by = 10
	template_name = 'administration/qualification_list.html'
	form_class =Qualification

	def get_context_data(self, **kwargs):
		context = super(QualificationList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] =QualificationSearchForm(self.request.GET)
		else:
			context['form'] =QualificationSearchForm()
		return context

	def get_queryset(self):
		args = {}
		qualification_id= self.request.GET.get('qualification_id',False)
		qualification_name=self.request.GET.get('qualification_name',False)
	   

		if qualification_name:
			args['qualification_name__icontains'] =qualification_name

		if qualification_name:
			args['qualification_name__icontains'] = qualification_name

		return 	Qualification.objects.filter(**args).order_by('-qualification_id')

class QualificationCreate(CreateView):
	model = Qualification
	form_class = QualificationForm
	template_name = "administration/qualification_form.html"
	success_url = reverse_lazy('qualification')
	

	def get_context_data(self, **kwargs):
		context = super(QualificationCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class QualificationUpdate(UpdateView):
	model = Qualification
	form_class = QualificationForm
	template_name="administration/qualification_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('education')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class QualificationDetail(DetailView):
	model = Qualification
	template_name = "administration/qualifaction_detail.html"




class OccupationList(ListView):
	model = Occupation
	paginate_by = 10
	template_name = 'administration/occupation_list.html'
	form_class =Occupation

	def get_context_data(self, **kwargs):
		context = super(OccupationList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] =OccupationSearchForm(self.request.GET)
		else:
			context['form'] =OccupationSearchForm()
		return context

	def get_queryset(self):
		args = {}
		occupation_id= self.request.GET.get('occupation_id',False)
		occupation_name=self.request.GET.get('occupation_name',False)
	   

		if occupation_id:
			args['occupation_id__icontains'] =occupation_id

		if occupation_name:
			args['occupation_id__icontains'] = occupation_name

		return 	Occupation.objects.filter(**args).order_by('-occupation_id')

class OccupationCreate(CreateView):
	model = Occupation
	form_class = OccupationForm
	template_name = "administration/occupation_form.html"
	success_url = reverse_lazy('occupation')
	

	def get_context_data(self, **kwargs):
		context = super(OccupationCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class OccupationUpdate(UpdateView):
	model = Occupation
	form_class = OccupationForm
	template_name="administration/qualification_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('occupation')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class OccupationDetail(DetailView):
	model = Occupation
	template_name = "administration/occupation_detail.html"



class SubOccupationList(ListView):
	model = SubOccupation
	paginate_by = 10
	template_name = 'administration/suboccupation_list.html'
	form_class =SubOccupation

	def get_context_data(self, **kwargs):
		context = super(SubOccupationList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] =SubOccupationSearchForm(self.request.GET)
		else:
			context['form'] =SubOccupationSearchForm()
		return context

	def get_queryset(self):
		args = {}
		suboccupation_id= self.request.GET.get('suboccupation_id',False)
		suboccupation_name=self.request.GET.get('suboccupation_name',False)
		occupation_id=self.request.GET.get('occupation_id',False)
	   

		if suboccupation_id:
			args['suboccupation_id__icontains'] =suboccupation_id

		if suboccupation_name:
			args['suboccupation_name__icontains'] = suboccupation_name

		if occupation_id:
			args['occupation_id__icontains'] =occupation_id

		return 	SubOccupation.objects.filter(**args).order_by('-suboccupation_id')

class SubOccupationCreate(CreateView):
	model = SubOccupation
	form_class = SubOccupationForm
	template_name = "administration/suboccupation_form.html"
	success_url = reverse_lazy('suboccupation')
	

	def get_context_data(self, **kwargs):
		context = super(SubOccupationCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class SubOccupationUpdate(UpdateView):
	model = SubOccupation
	form_class = SubOccupationForm
	template_name="administration/suboccupation_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('suboccupation')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class SubOccupationDetail(DetailView):
	model = SubOccupation
	template_name = "administration/suboccupation_detail.html"



class CountryList(ListView):
	model = Country
	paginate_by = 10
	template_name = 'administration/country_list.html'
	form_class =Country

	def get_context_data(self, **kwargs):
		context = super(CountryList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] =CountrySearchForm(self.request.GET)
		else:
			context['form'] =CountrySearchForm()
		return context

	def get_queryset(self):
		args = {}
		country_id= self.request.GET.get('country_id',False)
		country_name=self.request.GET.get('country_name',False)
		
	   

		if country_id:
			args['country_id__icontains'] =country_id

		if country_name:
			args['country_name__icontains'] = country_name


		return 	Country.objects.filter(**args).order_by('-country_id')

class CountryCreate(CreateView):
	model = Country
	form_class = CountryForm
	template_name = "administration/country_form.html"
	success_url = reverse_lazy('country')
	

	def get_context_data(self, **kwargs):
		context = super(CountryCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class CountryUpdate(UpdateView):
	model = Country
	form_class = CountryForm
	template_name="administration/country_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('country')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class CountryDetail(DetailView):
	model = Country
	template_name = "administration/country_detail.html"



class StateList(ListView):
	model = State
	paginate_by = 10
	template_name = 'administration/state_list.html'
	form_class =State

	def get_context_data(self, **kwargs):
		context = super(StateList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] =StateSearchForm(self.request.GET)
		else:
			context['form'] =StateSearchForm()
		return context

	def get_queryset(self):
		args = {}
		state_id= self.request.GET.get('state_id',False)
		state_name=self.request.GET.get('state_name',False)
		country_id=self.request.GET.get('country_id',False)
		
		
	   

		if state_id:
			args['state_id__icontains'] =state_id

		if state_name:
			args['state_name__icontains'] = state_name

		
		if country_id:
			args['country_id__icontains'] = country_id


		return 	State.objects.filter(**args).order_by('-state_id')

class StateCreate(CreateView):
	model = State
	form_class = StateForm
	template_name = "administration/state_form.html"
	success_url = reverse_lazy('state')
	

	def get_context_data(self, **kwargs):
		context = super(StateCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class StateUpdate(UpdateView):
	model = State
	form_class = StateForm
	template_name="administration/state_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('state')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class StateDetail(DetailView):
	model = State
	template_name = "administration/state_detail.html"




class DistrictList(ListView):
	model = District
	paginate_by = 10
	template_name = 'administration/district_list.html'
	form_class =District

	def get_context_data(self, **kwargs):
		context = super(DistrictList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] =DistrictSearchForm(self.request.GET)
		else:
			context['form'] =DistrictSearchForm()
		return context

	def get_queryset(self):
		args = {}
		district_id= self.request.GET.get('district_id',False)
		district_name=self.request.GET.get('district_name',False)
		state_id=self.request.GET.get('state_id',False)
		
		
	   

		if district_id:
			args['district_id__icontains'] =district_id

		if district_name:
			args['district_name__icontains'] = district_name

		
		if state_id:
			args['state_id__icontains'] = state_id


		return 	District.objects.filter(**args).order_by('-district_id')

class DistrictCreate(CreateView):
	model = District
	form_class = DistrictForm
	template_name = "administration/district_form.html"
	success_url = reverse_lazy('district')
	

	def get_context_data(self, **kwargs):
		context = super(DistrictCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class DistrictUpdate(UpdateView):
	model = District
	form_class = DistrictForm
	template_name="administration/district_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('district')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class DistrictDetail(DetailView):
	model = District
	template_name = "administration/district_detail.html"




class CityList(ListView):
	model = City
	paginate_by = 10
	template_name = 'administration/city_list.html'
	form_class =City

	def get_context_data(self, **kwargs):
		context = super(CityList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] =CitySearchForm(self.request.GET)
		else:
			context['form'] =CitySearchForm()
		return context

	def get_queryset(self):
		args = {}
		city_id= self.request.GET.get('city_id',False)
		city_name=self.request.GET.get('city_name',False)
		district_id=self.request.GET.get('district_id',False)
		
		
	   

		if city_id:
			args['city_id__icontains'] =city_id

		if city_name:
			args['city_name__icontains'] = city_name

		
		if district_id:
			args['district_id__icontains'] = district_id


		return 	City.objects.filter(**args).order_by('-city_id')

class CityCreate(CreateView):
	model = City
	form_class = CityForm
	template_name = "administration/city_form.html"
	success_url = reverse_lazy('city')
	

	def get_context_data(self, **kwargs):
		context = super(CityCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 

class CityUpdate(UpdateView):
	model = City
	form_class = CityForm
	template_name="administration/city_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('city')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class CityDetail(DetailView):
	model = City
	template_name = "administration/city_detail.html"
