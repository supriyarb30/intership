from django.forms import ModelForm, Textarea, TextInput
from django import forms
from .models import *



class FeedbackSearchForm(forms.Form):
	feedback_id=forms.IntegerField(required=False)
	feedback_name= forms.CharField(required=False)
	email= forms.CharField(required=False)
	mobile= forms.CharField(required=False)
	address= forms.CharField(required=False)
	subject= forms.CharField(required=False)
	suggestions= forms.CharField(required=False)