from django.forms import ModelForm
from django import forms
from core.models import *
from .forms import *


class FeedbackForm(ModelForm):
    class Meta:
        model = Feedback
        fields = ('feedback_id','feedback_name','email','mobile','address','subject','suggestions')
        # exclude = ('person')