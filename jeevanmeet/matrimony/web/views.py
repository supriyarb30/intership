from django.shortcuts import render
from django.views.generic import TemplateView,CreateView,ListView,CreateView,UpdateView,DetailView
from core.models import *
from .search_forms import *
from django.urls import reverse
from .forms import *
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect

# Create your views here.
class Home(TemplateView):
	template_name = 'web/home.html'
	

class FeedbackList(ListView):
	model = Feedback
	paginate_by = 10
	template_name = 'web/feedback.html'
	form_class = FeedbackSearchForm

	def get_context_data(self, **kwargs):
		context = super(FeedbackList, self).get_context_data(**kwargs)
		if self.request.GET:
			context['form'] = FeedbackForm(self.request.GET)
		else:
			context['form'] = FeedbackSearchForm()
		return context

	def get_queryset(self):
		args = {}
		feedback_id = self.request.GET.get('feedback_id',False)
		feedback_name= self.request.GET.get('feedback_name',False)
		email = self.request.GET.get('email',False)
		address = self.request.GET.get('address',False)
		subject = self.request.GET.get('subject',False)
		suggestions = self.request.GET.get('suggestions',False)
		


	    

		if feedback_id:
			args['feedback_id'] = feedback_id
		
		if feedback_name:
			args['feedback_name'] = feedback_name

		if email:
			args['email']=email

		if address:
			args['address']=address

		if subject:
			args['subject']=subject

		if suggestions:
			args['suggestions']=suggestions

		return Feedback.objects.filter(**args).order_by('-feedback_id')

class FeedbackCreate(CreateView):
	model = Feedback
	form_class = FeedbackForm
	template_name = "web/feedback_form.html"
	success_url = reverse_lazy('feedback')

	def get_context_data(self, **kwargs):
		context = super(FCreate, self).get_context_data(**kwargs)
		return context

	def post(self, request, *args, **kwargs):
		self.object = None
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		
		if (form.is_valid()):
		   
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(self.get_context_data(form=form)) 
	
	
class FeedbackUpdate(UpdateView):
	model = Feedback
	form_class = FeedbackForm
	template_name="web/feedback_form.html"


	def get_success_url(self):
		print("VALID")
		return reverse('cast')

	def form_valid(self, form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form):
		print("INVALID")
		print(form)
		return self.render_to_response(
			self.get_context_data(form=form,
								  ))

class FeedbackDetail(DetailView):
	model = Caste
	template_name = "web/feedback_detail.html"


class MemebershipList(ListView):
	model = Feedback
	paginate_by = 10
	template_name = 'web/membership.html'
	#form_class = FeedbackSearchForm