from django.urls import path
from .views import *


urlpatterns = [

	    	path('',Home.as_view(), name='home'),
			

			path('feedback/', FeedbackList.as_view(), name='feedback'),
			path('add-feedback/', FeedbackCreate.as_view(), name='add-feedback'),
			path('edit-feedback/<int:pk>', FeedbackUpdate.as_view(), name='edit-feedback'),
     	    path('detail-feedback/<int:pk>', FeedbackDetail.as_view(), name='detail-feedback'),

			path('membership/', MemebershipList.as_view(), name='membership'),

]