from django.shortcuts import render

# Create your views here.
from api.models import *
from rest_framework.views import APIView
from django.core import serializers
from .serializers import *
from rest_framework.response import Response
from rest_framework import status



# tokan 
#from rest_framework.authtoken.models import Token
#from rest_framework.authentication import TokenAuthentication
#from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
#from rest_framework_simplejwt.views import TokenObtainPairView
#from rest_framework.authtoken.serializers import AuthTokenSerializer

from django.views.generic import CreateView,DetailView,DeleteView,UpdateView,ListView
#from .forms import *
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy

# Create your views here.






#class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
	#@classmethod
	#def get_token(cls, user):
		#token = super().get_token(user)
		#token['name'] = user.first_name
		#return token

#class MyTokenObtainPairView(TokenObtainPairView):
	#serializer_class = MyTokenObtainPairSerializer


class DemoAPI(APIView):

# POST API
    def post(self, request, format=None):
        data = request.data
        serializer = DemoSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# GET API
    def get(self,request,format=None):
        register = Demo.objects.all()
        serializer = DemoSerializer(register,many=True)
        return Response(serializer.data)


class DemoUpdate(APIView):
    def get_object(self,pk):
        try:
            return Demo.objects.get(pk=pk)
        except Demo.DoesNotExist:
                raise Http404


    def put(self, request,pk,format=None):
        demo_id = self.get_object(pk)
        serializer = DemoSerializer(demo_id, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class DemoDeleteAPI(APIView):
    def get_object(self, pk):
        try:
            return Demo.objects.get(pk=pk)
        except Demo.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        demo_id = self.get_object(pk)
        demo_id.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



class DetailDemoGetAPI(APIView):
    def get_object(self, pk):
        try:
            return Demo.objects.get(pk=pk)
        except Demo.DoesNotExist:
            return Response(status=404)
            
    def get(self, request, pk, format=None):
        try:
            demo_id = Demo.objects.get(pk=pk)
            serializer = DemoSerializer(demo_id)
            return Response(serializer.data)
        except Demo.DoesNotExist:
            return Response(status=404)
