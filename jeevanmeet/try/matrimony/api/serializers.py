from rest_framework import serializers
from .models import *




class DemoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Demo
        fields = ('demo_id','name','address','mobile_no')

