from django.db import models

# Create your models here.

class Demo(models.Model):
	demo_id= models.AutoField(primary_key=True)
	name = models.CharField(max_length=20)
	address = models.CharField(max_length=20)
	mobile_no=models.CharField(max_length=10)

	def __str__(self):
		return str(self.demo_id)