from django.urls import path,include
from rest_framework import routers
from api.views import *
#from rest_framework_simplejwt import views as jwt_views
from api import views

from . import views

#from . import reports_print

urlpatterns = [
			    path('demo/',DemoAPI.as_view(),name='demo'),
				path('edit-demo/<int:pk>',DemoUpdate.as_view(),name='edit-demo'),

				#path('edit-demo/<int:pk>',DemoUpdate.as_view(),name='edit-demo'),
				path('delete-demo/<int:pk>',DemoDeleteAPI.as_view(),name='delete-demo'),
				
				path('detail-demo/<int:pk>',DetailDemoGetAPI.as_view(),name='detail-demo'),


			    #path('token/', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    			#path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
