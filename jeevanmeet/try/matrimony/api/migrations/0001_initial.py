# Generated by Django 3.1 on 2021-03-17 12:23

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Demo',
            fields=[
                ('demo_id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=20)),
                ('address', models.CharField(max_length=20)),
                ('mobile_no', models.CharField(max_length=10)),
            ],
        ),
    ]
