from django.db import models
from core.models import models

# Create your models here.

class MotherTongue(models.Model):
	mothertongue_id = models.AutoField(primary_key=True)
	mothertongue_name = models.CharField(max_length=10)
	def __str__(self):
		return self.mothertongue_name

class Religion(models.Model):
	religion_id = models.AutoField(primary_key=True)
	religion_name = models.CharField(max_length=20)
	def __str__(self):
		return self.religion_name

class Caste(models.Model):
	caste_id = models.AutoField(primary_key=True)
	caste_name = models.CharField(max_length=20)
	religion_id = models.ForeignKey(Religion, on_delete=models.CASCADE)
	def __str__(self):
		return self.caste_name

class SubCaste(models.Model):
	subcaste_id = models.AutoField(primary_key=True)
	subcaste_name = models.CharField(max_length=20)
	caste_id = models.ForeignKey(Caste, on_delete=models.CASCADE)
	def __str__(self):
		return self.subcaste_name

class StarRashi(models.Model):
	rashi_id = models.AutoField(primary_key=True)
	rashi_name = models.CharField(max_length=20)
	def __str__(self):
		return self.rashi_name

class MaritalStatus(models.Model):
	maritalstatus_id = models.AutoField(primary_key=True)
	maritalstatus_name = models.CharField(max_length=25)
	def __str__(self):
		return self.maritalstatus_name

class Complexion(models.Model):
	complexion_id = models.AutoField(primary_key=True)
	complexion_name = models.CharField(max_length=25)
	def __str__(self):
		return self.complexion_name

class BloodGroup(models.Model):
	bloodgroup_id = models.AutoField(primary_key=True)
	bloodgroup_name = models.CharField(max_length=5)
	def __str__(self):
		return self.bloodgroup_name

class BodyType(models.Model):
	bodytype_id = models.AutoField(primary_key=True)
	bodytype_name = models.CharField(max_length=20)
	def __str__(self):
		return self.bodytype_name

class Education(models.Model):
	education_id = models.AutoField(primary_key=True)
	education_name = models.CharField(max_length=25)
	def __str__(self):
		return self.education_name

class Qualification(models.Model):
	qualification_id = models.AutoField(primary_key=True)
	qualification_name = models.CharField(max_length=25)
	education_id = models.ForeignKey(Education, on_delete=models.CASCADE)
	def __str__(self):
		return self.qualification_name

class Occupation(models.Model):
	occupation_id = models.AutoField(primary_key=True)
	occupation_name = models.CharField(max_length=25)
	def __str__(self):
		return self.occupation_name

class SubOccupation(models.Model):
	suboccupation_id = models.AutoField(primary_key=True)
	suboccupation_name = models.CharField(max_length=25)
	occupation_id = models.ForeignKey(Occupation, on_delete=models.CASCADE)
	def __str__(self):
		return self.suboccupation_name
		
class Country(models.Model):
	country_id = models.AutoField(primary_key=True)
	country_name = models.CharField(max_length=25)
	def __str__(self):
		return self.country_name

class State(models.Model):
	state_id = models.AutoField(primary_key=True)
	state_name = models.CharField(max_length=25)
	country_id = models.ForeignKey(Country, on_delete=models.CASCADE)
	def __str__(self):
		return self.state_name

class District(models.Model):
	district_id = models.AutoField(primary_key=True)
	district_name = models.CharField(max_length=25)
	state_id = models.ForeignKey(State, on_delete=models.CASCADE)
	def __str__(self):
		return self.district_name

class City(models.Model):
	city_id = models.AutoField(primary_key=True)
	city_name = models.CharField(max_length=25)
	district_id = models.ForeignKey(District, on_delete=models.CASCADE)
	def __str__(self):
		return self.city_name



######
class MemTypeMaster(models.Model):
	mem_type_id = models.AutoField(primary_key=True)
	desc_en = models.CharField('Description in English', max_length=100, unique=True)
	desc_ma = models.CharField('Description in Marathi', max_length=100, unique=True)

	def __str__(self):
		return self.desc_ma + '-' +self.desc_en

class DistrictMaster(models.Model):
	district_id = models.AutoField(primary_key=True)
	dist_en = models.CharField("Name in English", max_length=200, unique=True)
	dist_ma = models.CharField("Name in Marathi", max_length=200, unique=True)
	def __str__(self):
		return self.dist_ma + '-' +self.dist_en


class TalukaMaster(models.Model):
	taluka_id = models.AutoField(primary_key=True)
	taluka_en = models.CharField("Name in English", max_length=200, unique=True)
	taluka_ma = models.CharField("Name in Marathi", max_length=200, unique=True)
	district = models.ForeignKey(DistrictMaster, verbose_name='District Name',on_delete=models.CASCADE)
	def __str__(self):
		return self.taluka_ma + '-' +self.taluka_en


class VillageMaster(models.Model):
	village_id = models.AutoField(primary_key=True)
	village_en = models.CharField("Name in English", max_length=200, unique=True)
	village_ma = models.CharField("Name in Marathi", max_length=200, unique=True)
	taluka = models.ForeignKey(TalukaMaster, verbose_name='Taluka Name',on_delete=models.CASCADE)

	class Meta:
		ordering = ['village_en']

	def __str__(self):
		return str(self.village_id) + ' - ' + self.village_ma

class PersonMaster(models.Model):
	GENDER = (('M', 'Male'), ('F', 'Female'))
	person_id = models.AutoField(primary_key=True)
	fname_en = models.CharField(max_length=255)
	mname_en = models.CharField(max_length=255)
	lname_en = models.CharField(max_length=255)
	alias_en = models.CharField(max_length=255, blank=True)
	fname_ma = models.CharField(max_length=255)
	mname_ma = models.CharField(max_length=255)
	lname_ma = models.CharField(max_length=255)
	alias_ma = models.CharField(max_length=255, blank=True)
	gender = models.CharField(max_length=1, choices=GENDER)
	birth_date = models.DateField(blank=True, null=True)
	mobile_no = models.CharField(max_length=12, blank=True)
	phone_no = models.CharField(max_length=20, blank=True)
	fax = models.CharField(max_length=15, blank=True)
	email_id = models.CharField(max_length=255, blank=True)
	village = models.ForeignKey(VillageMaster, on_delete=models.CASCADE)
	aadhar_number = models.CharField(max_length=20, blank=True, unique=True)
	pan_no = models.CharField(max_length=30, blank=True)
	entity_guid = models.CharField(max_length=64, unique=True, blank=True)
	main_person_id = models.IntegerField(blank=True, null=True)
	status = models.CharField(max_length=1, default='A')

	def __str__(self):
		return str(self.person_id)

	

class Sample(models.Model):
	sr_no = models.AutoField(primary_key=True)
	person = models.OneToOneField(PersonMaster, on_delete=models.CASCADE,null=True,blank=True)
	code = models.IntegerField()
	full_name_ma = models.CharField(max_length=50)
	f_name_ma = models.CharField(max_length=20)
	m_name_ma = models.CharField(max_length=20)
	l_name_ma = models.CharField(max_length=20)
	full_name_en = models.CharField(max_length=50)
	f_name_en = models.CharField(max_length=20)
	m_name_en = models.CharField(max_length=20)
	l_name_en = models.CharField(max_length=20)
	village_ma = models.ForeignKey(VillageMaster, on_delete=models.CASCADE,related_name='village_m')
	village_en = models.ForeignKey(VillageMaster, on_delete=models.CASCADE,related_name='village_e')
	taluka_ma = models.ForeignKey(TalukaMaster, on_delete=models.CASCADE,related_name='taluka_m')
	taluka_en = models.ForeignKey(TalukaMaster, on_delete=models.CASCADE,related_name='taluka_e')
	dist_ma = models.ForeignKey(DistrictMaster, on_delete=models.CASCADE,related_name='dist_m')
	dist_en = models.ForeignKey(DistrictMaster, on_delete=models.CASCADE,related_name='dist_e')
	mem_type = models.ForeignKey(MemTypeMaster, on_delete=models.CASCADE)
	mob_no = models.CharField(max_length=13)

	def __str__(self):
		return self.full_name_ma




