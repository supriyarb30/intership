from django.forms import ModelForm, Textarea, TextInput
from django import forms
from .models import *


class SampleSearchForm(forms.Form):
	code=forms.IntegerField(required=False)
	full_name_ma= forms.CharField(required=False)
	full_name_en= forms.CharField(required=False)
	
	village_en = forms.ModelChoiceField(
				queryset= VillageMaster.objects.all(),
				required=False
		)
	
	taluka_en = forms.ModelChoiceField(
				queryset= TalukaMaster.objects.all(),
				required=False
		)
	
	dist_en = forms.ModelChoiceField(
				queryset= DistrictMaster.objects.all(),
				required=False
		)
	
	 