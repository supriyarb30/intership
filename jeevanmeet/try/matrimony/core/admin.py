from django.contrib import admin
from core.models import *
# Register your models here.

admin.site.register(MotherTongue)
admin.site.register(Caste)
admin.site.register(Religion)
admin.site.register(SubCaste)
admin.site.register(StarRashi)
admin.site.register(MaritalStatus)
admin.site.register(Complexion)
admin.site.register(BloodGroup)
admin.site.register(BodyType)
admin.site.register(Education)
admin.site.register(Qualification)
admin.site.register(Occupation)
admin.site.register(SubOccupation)
admin.site.register(Country)
admin.site.register(State)
admin.site.register(District)
admin.site.register(City)