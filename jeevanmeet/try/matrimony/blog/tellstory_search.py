from django.forms import ModelForm, Textarea, TextInput
from django import forms
from .models import *


class StorySearchForm(forms.Form):
	your_name=forms.IntegerField(required=False)
	partner_name=forms.CharField(required=False)
	wedding_date=forms.CharField(required=False)
	photo=forms.CharField(required=False)

