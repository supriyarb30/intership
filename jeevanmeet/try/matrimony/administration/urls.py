from django.urls import path
from .views import *
from franchise.views import *
from .import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
            path('dashbord/',AdminHome.as_view(), name='dashbord'),

            path('sample/', SampleList.as_view(), name='sample'),
     	    path('add-sample/', SampleCreate.as_view(), name='add-sample'),
     	    path('edit-sample/<int:pk>', SampleUpdate.as_view(), name='edit-sample'),
     	    path('detail-sample/<int:pk>', SampleDetail.as_view(), name='detail-sample'),

			path('cast/', CastList.as_view(), name='cast'),
			path('add-cast/', CastCreate.as_view(), name='add-cast'),
			path('edit-cast/<int:pk>', CastUpdate.as_view(), name='edit-cast'),
     	    path('detail-cast/<int:pk>', CastDetail.as_view(), name='detail-cast'),

            path('religion/', ReligionList.as_view(), name='religion'),
            path('add-religion/', ReligionCreate.as_view(), name='add-religion'),
			path('edit-religion/<int:pk>', ReligionUpdate.as_view(), name='edit-religion'),
     	    path('detail-religion/<int:pk>', ReligionDetail.as_view(), name='detail-religion'),
           
            path('mothertongue/', MotherTongueList.as_view(), name='mothertongue'),
            path('add-mothertongue/', MotherTongueCreate.as_view(), name='add-mothertongue'),
			path('edit-mothertongue/<int:pk>', MotherTongueUpdate.as_view(), name='edit-mothertongue'),
     	    path('detail-mothertongue/<int:pk>', MotherTongueDetail.as_view(), name='detail-mothertongue'),
           
	        path('subcaste/',SubCasteList.as_view(), name='subcaste'),
            path('add-subcaste/', SubCasteCreate.as_view(), name='add-subcaste'),
			path('edit-subcaste/<int:pk>', SubCasteUpdate.as_view(), name='edit-subcaste'),
     	    path('detail-subcaste/<int:pk>', SubCasteDetail.as_view(), name='detail-subcaste'),
			
			path('starrashi/',StarRashiList.as_view(), name='starrashi'),
            path('add-starrashi/', StarRashiCreate.as_view(), name='add-starrashi'),
			path('edit-starrashi/<int:pk>', StarRashiUpdate.as_view(), name='edit-starrashi'),
     	    path('detail-starrashi/<int:pk>', StarRashiDetail.as_view(), name='detail-starrashi'),

			path('maritalstatus/',MaritalStatusList.as_view(), name='maritalstatus'),
            path('add-maritalstatus/', MaritalStatusCreate.as_view(), name='add-maritalstatus'),
			path('edit-maritalstatus/<int:pk>', MaritalStatusUpdate.as_view(), name='edit-maritalstatus'),
     	    path('detail-maritalstatus/<int:pk>', MaritalStatusDetail.as_view(), name='detail-maritalstatus'),

			path('complexion/',ComplexionList.as_view(), name='complexion'),
            path('add-complexion/', ComplexionCreate.as_view(), name='add-complexion'),
			path('edit-complexion/<int:pk>',ComplexionUpdate.as_view(), name='edit-complexion'),
     	    path('detail-complexion<int:pk>', ComplexionDetail.as_view(), name='detail-complexion'),

            path('bloodgroup/',BloodGroupList.as_view(), name='bloodgroup'),
            path('add-bloodgroup/',BloodGroupCreate.as_view(), name='add-bloodgroup'),
			path('edit-bloodgroup/<int:pk>',BloodGroupUpdate.as_view(), name='edit-bloodgroup'),
     	    path('detail-bloodgroup<int:pk>', BloodGroupDetail.as_view(), name='detail-bloodgroup'),

            path('bodytype/',BodyTypeList.as_view(), name='bodytype'),
            path('add-bodytype/',BodyTypeCreate.as_view(), name='add-bodytype'),
			path('edit-bodytype/<int:pk>',BodyTypeUpdate.as_view(), name='edit-bodytype'),
     	    path('detail-bodytype<int:pk>', BodyTypeDetail.as_view(), name='detail-bodytype'),
			 
			path('education/',EducationList.as_view(), name='education'),
            path('add-education/',EducationCreate.as_view(), name='add-education'),
			path('edit-education/<int:pk>',EducationUpdate.as_view(), name='edit-education'),
     	    path('detail-education<int:pk>', EducationDetail.as_view(), name='detail-education'),

			path('qualification/',QualificationList.as_view(), name='qualification'),
            path('add-qualification/',QualificationCreate.as_view(), name='add-qualification'),
			path('edit-qualification/<int:pk>',QualificationUpdate.as_view(), name='edit-qualification'),
     	    path('detail-qualification<int:pk>', QualificationDetail.as_view(), name='detail-qualification'),

            path('occupation/',OccupationList.as_view(), name='occupation'),
            path('add-occupation/',OccupationCreate.as_view(), name='add-occupation'),
			path('edit-occupation/<int:pk>',OccupationUpdate.as_view(), name='edit-occupation'),
     	    path('detail-occupation<int:pk>', OccupationDetail.as_view(), name='detail-occupation'),

			path('suboccupation/',SubOccupationList.as_view(), name='suboccupation'),
            path('add-suboccupation/',SubOccupationCreate.as_view(), name='add-suboccupation'),
			path('edit-suboccupation/<int:pk>',SubOccupationUpdate.as_view(), name='edit-suboccupation'),
     	    path('detail-suboccupation<int:pk>', SubOccupationDetail.as_view(), name='detail-suboccupation'),

			path('country/',CountryList.as_view(), name='country'),
            path('add-country/',CountryCreate.as_view(), name='add-country'),
			path('edit-country/<int:pk>',CountryUpdate.as_view(), name='edit-country'),
     	    path('detail-country<int:pk>',CountryDetail.as_view(), name='detail-country'),

			path('state/',DistrictList.as_view(), name='state'),
            path('add-state/',DistrictCreate.as_view(), name='add-state'),
			path('edit-state/<int:pk>',DistrictUpdate.as_view(), name='edit-state'),
     	    path('detail-state<int:pk>',DistrictDetail.as_view(), name='detail-state'), 
			 
			path('district/',DistrictList.as_view(), name='district'),
            path('add-district/',DistrictCreate.as_view(), name='add-district'),
			path('edit-district/<int:pk>',DistrictUpdate.as_view(), name='edit-district'),
     	    path('detail-district<int:pk>',DistrictDetail.as_view(), name='detail-district'), 

			path('city/',CityList.as_view(), name='city'),
            path('add-city/',CityCreate.as_view(), name='add-city'),
			path('edit-city/<int:pk>',CityUpdate.as_view(), name='edit-city'),
     	    path('detail-city<int:pk>',CityDetail.as_view(), name='detail-city'), 


            path('franchiseregister/', FranchiseRegisterList.as_view(), name='franchiseregister'),
			 
			


]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)