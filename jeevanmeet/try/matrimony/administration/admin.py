from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(MemTypeMaster)
admin.site.register(DistrictMaster)
admin.site.register(TalukaMaster)
admin.site.register(VillageMaster)
admin.site.register(Sample)
