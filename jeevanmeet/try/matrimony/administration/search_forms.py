from django.forms import ModelForm, Textarea, TextInput
from django import forms
from .models import *


class SampleSearchForm(forms.Form):
	code=forms.IntegerField(required=False)
	full_name_ma= forms.CharField(required=False)
	full_name_en= forms.CharField(required=False)
	
	village_en = forms.ModelChoiceField(
				queryset= VillageMaster.objects.all(),
				required=False
		)
	
	taluka_en = forms.ModelChoiceField(
				queryset= TalukaMaster.objects.all(),
				required=False
		)
	
	dist_en = forms.ModelChoiceField(
				queryset= DistrictMaster.objects.all(),
				required=False
		)


class CastSearchForm(forms.Form):
	caste_id=forms.IntegerField(required=False)
	caste_name= forms.CharField(required=False)



class ReligionSearchForm(forms.Form):
	religion_id=forms.IntegerField(required=False)
	religion_name= forms.CharField(required=False)

class MotherTongueSearchForm(forms.Form):
	mothertongue_id=forms.IntegerField(required=False)
	mothertongue_name= forms.CharField(required=False)

class SubCasteSearchForm(forms.Form):
	subcaste_id=forms.IntegerField(required=False)
	subcaste_name=forms.CharField(required=False)


class StarRashiSearchForm(forms.Form):
	rashi_id=forms.IntegerField(required=False)
	rashi_name=forms.CharField(required=False)

class MaritalStatusSearchForm(forms.Form):
	maritalstatus_id=forms.IntegerField(required=False)
	maritalstatus_name=forms.CharField(required=False)

class ComplexionSearchForm(forms.Form):
	complexion_id=forms.IntegerField(required=False)
	complexion_name=forms.CharField(required=False)

class BloodGroupSearchForm(forms.Form):
	bloodgroup_id=forms.IntegerField(required=False)
	bloodgroup_name=forms.CharField(required=False)

class BodyTypeSearchForm(forms.Form):
	bodytype_id=forms.IntegerField(required=False)
	bodytype_name=forms.CharField(required=False)

class EducationSearchForm(forms.Form):
	education_id=forms.IntegerField(required=False)
	education_name=forms.CharField(required=False)

class QualificationSearchForm(forms.Form):
	qualification_id=forms.IntegerField(required=False)
	qualification_name= forms.CharField(required=False)

class OccupationSearchForm(forms.Form):
	occupation_id=forms.IntegerField(required=False)
	occupation_name= forms.CharField(required=False)

class SubCasteSearchForm(forms.Form):
	suboccupation_id=forms.IntegerField(required=False)
	suboccupation_name=forms.CharField(required=False)
	occupation_id=forms.IntegerField(required=False)

    

class CountrySearchForm(forms.Form):
	country_id=forms.IntegerField(required=False)
	country_name=forms.CharField(required=False)


class StateSearchForm(forms.Form):
	state_id=forms.IntegerField(required=False)
	state_name=forms.CharField(required=False)

class DistrictSearchForm(forms.Form):
	district_id=forms.IntegerField(required=False)
	district_name=forms.CharField(required=False)

class CitySearchForm(forms.Form):
	city_id=forms.IntegerField(required=False)
	city_name= forms.CharField(required=False)





