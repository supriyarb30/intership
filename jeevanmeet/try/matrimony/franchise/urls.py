from django.urls import path
from .views import *
from .import views




urlpatterns = [
            
            path('franchiseregister/', FranchiseRegisterList.as_view(), name='franchiseregister'),
     	    path('add-franchiseregister/', FranchiseRegisterCreate.as_view(), name='add-franchiseregister'),
     	    path('edit-franchiseregister/<int:pk>', FranchiseRegisterUpdate.as_view(), name='edit-franchiseregister'),
     	    path('detail-franchiseregister/<int:pk>', FranchiseRegisterDetail.as_view(), name='detail-franchiseregister'),

]