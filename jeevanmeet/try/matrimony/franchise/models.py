from django.db import models
from franchise.models import models
from core.models import *




# Create your models here.
class FranchiseRegister(models.Model):
	email = models.EmailField(max_length=30)
	password = models.CharField(max_length=25)
	repassword = models.CharField(max_length=25)
	franchise_name = models.CharField(max_length=50)
	owner_name = models.CharField(max_length=30)
	mobile_number = models.CharField(max_length=10)
	mobile_number2 = models.CharField(max_length=10)
	country = models.ForeignKey(Country, on_delete=models.CASCADE)
	state = models.ForeignKey(State, on_delete=models.CASCADE)
	district = models.ForeignKey(District, on_delete=models.CASCADE)
	city = models.ForeignKey(City, on_delete=models.CASCADE)
	current_address = models.CharField(max_length=50)
	images = models.ImageField(upload_to='images/')
	def __str__(self):
		return self.franchise_name
