# Generated by Django 3.1 on 2021-01-23 09:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FranchiseRegister',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=30)),
                ('password', models.CharField(max_length=25)),
                ('repassword', models.CharField(max_length=25)),
                ('franchise_name', models.CharField(max_length=50)),
                ('owner_name', models.CharField(max_length=30)),
                ('mobile_number', models.CharField(max_length=10)),
                ('mobile_number2', models.CharField(max_length=10)),
                ('current_address', models.CharField(max_length=50)),
                ('images', models.ImageField(upload_to='images/')),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.city')),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.country')),
                ('district', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.district')),
                ('state', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.state')),
            ],
        ),
    ]
