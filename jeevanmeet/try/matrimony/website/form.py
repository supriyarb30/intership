from django import forms
from JeevanApp.models import *
from django.forms import widgets
from JeevanApp.autocomplete_views import *
from django_bleach.forms import BleachField

# from JeevanApp.autocomplete_views import *
from django.contrib.auth.forms import AuthenticationForm

class PersonalDetailsForm(forms.ModelForm):

    EATING_CHOICES = [('Vegetarian','Vegetarian'),('Non-Vegetarian','Non-Vegetarian'),
                      ('Eggetarian','Eggetarian')]
    eating_habits = forms.ChoiceField(choices=EATING_CHOICES, widget=forms.RadioSelect)

    DRINKING_CHOICES = [('No','No'),('Occassionally','Occassionally'),
                      ('Yes','Yes')]
    drinking_habits = forms.ChoiceField(choices=DRINKING_CHOICES, widget=forms.RadioSelect)

    SMOKING_CHOICES = [('No','No'),('Occassionally','Occassionally'),
                      ('Yes','Yes')]
    smoking_habits = forms.ChoiceField(choices=SMOKING_CHOICES, widget=forms.RadioSelect)

    PRT_EATING_CHOICES=(('Vegetarian','Vegetarian'),('Non-Vegetarian','Non-Vegetarian'),
                        ('Eggetarian','Eggetarian'))
    prt_eating_habits = forms.MultipleChoiceField(choices=PRT_EATING_CHOICES, widget=forms.CheckboxSelectMultiple)

    PRT_DRINKING_CHOICES = [('No','No'),('Occassionally','Occassionally'),
                      ('Yes','Yes')]
    prt_drinking_habits = forms.MultipleChoiceField(choices=PRT_DRINKING_CHOICES, widget=forms.CheckboxSelectMultiple)

    PRT_SMOKING_CHOICES = [('No','No'),('Occassionally','Occassionally'),
                      ('Yes','Yes')]
    prt_smoking_habits = forms.MultipleChoiceField(choices=PRT_SMOKING_CHOICES, widget=forms.CheckboxSelectMultiple)

    class Meta:
        model = PersonalDetails
        fields = '__all__'
        exclude = ['users',]