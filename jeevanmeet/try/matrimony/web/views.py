from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect


# Create your views here.
class Home(TemplateView):
    template_name = 'web/home.html'
